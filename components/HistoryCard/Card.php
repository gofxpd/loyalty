<?php

include_once __DIR__ . '/../../func/function.php';

class Card {

  private $history;

  public function __construct() {
    $this->history = new History();
  } 

  public function HistoryCard($row, $result, $user_email) {  
    $awd = $this->history->hisAward($result->reward_items_id, $user_email);
    $point = $awd['rewards_level_point'];    
    $data = json_decode($awd['value']);
    $img = json_decode($awd['reward_items_img']);
    $setImg = ($data->reward_items_image ? $data->reward_items_image : 'https://loyalty.gofx.com:2096/storage/'.$img->img0);
 
    return "<div class='row history-row'>
              <div class='col-lg-3 col-md-3 col-12'>
                <img src='$setImg' class='img-fulid history-img'>
              </div>
              <div class='col-lg-6 col-md-6 col-12'>
                <div class='row'>
                  <div class='col-md-12 w-card'><h3 class='history-reward-title-1 mgb-1 mgt-05'>".$data->reward_items_name."</h3></div>
                  <div class='col-md-12 w-card'><h4 class='history-reward-title-2 mgb-05'>"._HISTORY_BOX_TEXT_1."</h4></div>
                  <div class='col-md-12 w-card'><p class='history-reward-point'><img src='$_ENV[APP_STORAGE]/images/point.png' class='img-fulid reward-detail-point-img'>".number_format($point).' '._HISTORY_BOX_TEXT_2." </p></div>
                </div>
              </div>
              <div class='col-lg-3 col-md-3 col-12'>
                <div class='row'>
                  <div class='col-md-12 w-card'><p class='history-date-title'>"._HISTORY_BOX_TEXT_3.' '.date('d.m.Y', strtotime($row['created_at']))."</p></div>
                  <div class='col-md-12 w-card'><h4 class='history-reward-title-2 mgb-04'>สถานะ</h4></div>
                  <div class='col-md-12 w-card'><p class='history-reward-point'>".$this->historyStatus($result->history_status)."</p></div>
                </div>
              </div>
            </div>";
  }

  public function ResetCard($row, $result) {
    return "<div class='row history-row'>
              <div class='col-lg-3 col-md-3 col-12 d-flex'>
                <img src='$_ENV[APP_URL]/images/jj.png' class='img-fulid history-img' style='width: 110px'>
              </div>
              <div class='col-lg-6 col-md-6 col-12'>
                <h3 class='history-reward-title-1 mgb-1 mgt-05'>" . _HISTORY_RESET_TITLE . "</h3>
                <p>" . _HISTORY_RESET_DESC . "</p>
                <p class='history-reward-point'><img src='$_ENV[APP_STORAGE]/images/point.png' class='img-fulid reward-detail-point-img'>-".number_format($result->old_point - $result->new_point).' '._HISTORY_BOX_TEXT_2." </p>
              </div>
              <div class='col-lg-3 col-md-3 col-12'>
                <p class='history-date-title'>"._HISTORY_BOX_TEXT_4.' '.date('d.m.Y', strtotime($row['created_at']))."</p>
              </div>
            </div>";
  }

  public function CustomLotCard($row, $result) {
    return "<div class='row history-row'>
            <div class='col-lg-3 col-md-3 col-12 d-flex'>
              <img src='$_ENV[APP_URL]/images/jj.png' class='img-fulid history-img' style='width: 110px'>
            </div>
            <div class='col-lg-6 col-md-6 col-12'>
              <h3 class='history-reward-title-1 mgb-1 mgt-05'>" . _HISTORY_CUSTOM_LOT_TITLE . "</h3>
              <p>" . _HISTORY_CUSTOM_LOT_DESC . "</p>
              <p class='history-reward-point'><img src='$_ENV[APP_STORAGE]/images/point.png' class='img-fulid reward-detail-point-img'>".number_format($result->new_point - $result->old_point).' '._HISTORY_BOX_TEXT_2." </p>
            </div>
            <div class='col-lg-3 col-md-3 col-12'>
              <p class='history-date-title'>"._HISTORY_BOX_TEXT_4.' '.date('d.m.Y', strtotime($row['created_at']))."</p>
            </div>
          </div>";
  }

  private function historyStatus($status) {
    switch ($status) {
      case 1: return "<label class='text-pending'>Pending</label>";
      case 2: return "<label class='text-progress'>In progress</label>";
      case 3: return "<label class='text-success'>Success</label>";
      case 4: return "<label class='text-danger'>Cancel</label>";
    }
  }
  
}