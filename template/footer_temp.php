<!-- <div class="social-icons" id="s-icons">
  <button class="btn btn-danger shadow btn-social" id="btn-social" style="margin-left: 20px; outline:none;">
    <i class="fas fa-comment-dots" style="font-size: 1.5rem;color: white"></i>
  </button>
</div> -->
<div class="social-icons" id="s-icons">
	<ul class="navbar-nav navbar-nav-icon" style="list-style: none">
		<li><a target="_blank" href="https://web.facebook.com/GOFXThailand" class="btn btn-danger btn-social text-white fb"><i class="fab fa-facebook-f" style="font-size: 1.5rem;color: white"></i></a></li>
		<li><a target="_blank" href="https://twitter.com/gofxthailand" class="btn btn-danger btn-social text-white twitter"><i class="fab fa-twitter" style="font-size: 1.5rem;color: white"></i></a></li>
		<li><a target="_blank" href=" https://www.instagram.com/gofx_thailand/?hl=th" class="btn btn-danger btn-social text-white ig"><i class="fab fa-instagram" style="font-size: 1.5rem;color: white"></i></a></li>
		<li><a target="_blank" href=" https://www.youtube.com/channel/UCrKNDDHpE1hR3ibZCo6gEmg/featured" class="btn btn-danger btn-social text-white youtube"><i class="fab fa-youtube" style="font-size: 1.5rem;color: white"></i></a></li>
	</ul>
	<button class="btn btn-danger shadow btn-social" id="btn-social" style="margin-left: 20px;">
		<i class="fas fa-ellipsis-h text-white" style="font-size: 1.5rem;color: white"></i>
	</button>
	<button class="btn btn-danger shadow btn-social" id="btn-livechat" style="margin-left: 20px;">
		<i class="fas fa-comment-dots text-white" style="font-size: 1.5rem;color: white"></i>
	</button>
</div>
<!-- Footer Section Begin -->
<footer class="footer spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-4 col-12">
              <div class="footer__widget">
                  <h6><?= _FOOTER_MAIN1 ?></h6>
                  <ul>
                      <li><a href="https://www.gofx.com/account-type/" target="_blank"><?= _FOOTER_MAIN1_1 ?></a></li>
                  </ul>
              </div>
            </div>
            <div class="col-lg-4 col-md-8 col-12">
              <div class="footer__widget footer__widget_1">
                  <h6><?= _FOOTER_MAIN2 ?></h6>
                  <ul>
                      <li><a href="https://www.gofx.com/products/forex/go-standard/#forex" target="_blank"><?= _FOOTER_MAIN2_1 ?></a></li>
                      <li><a href="https://www.gofx.com/products/cfd-trading-other/go-inter/#other" target="_blank"><?= _FOOTER_MAIN2_2 ?></a></li>
                      <li><a href="https://www.gofx.com/products/cfd-trading-energy/go-standard/#energy" target="_blank"><?= _FOOTER_MAIN2_3 ?></a></li>
                  </ul>
                  <ul>
                      <li><a href="https://www.gofx.com/products/cfd-trading-metals/go-standard/#precious-metals" target="_blank"><?= _FOOTER_MAIN2_4 ?></a></li>
                      <li><a href="https://www.gofx.com/products/cfd-trading-stocks/go-inter/#stocks" target="_blank"><?= _FOOTER_MAIN2_5 ?></a></li>
                      <li><a href="https://www.gofx.com/products/cfd-trading-cryptocurrencies/go-low-spread/#cryptocurrency" target="_blank"><?= _FOOTER_MAIN2_6 ?></a></li>
                  </ul>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-6">
              <div class="footer__widget">
                  <h6><?= _FOOTER_MAIN3 ?></h6>
                  <ul>
                      <li><a href="https://www.gofx.com/privacy-policy/" target="_blank"><?= _FOOTER_MAIN3_1 ?></a></li>
                      <li><a href="https://www.gofx.com/terms-and-conditions/" target="_blank"><?= _FOOTER_MAIN3_2 ?></a></li>
                      <li><a href="https://www.gofx.com/trading-time/" target="_blank"><?= _FOOTER_MAIN3_3 ?></a></li>
                  </ul>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-6">
              <div class="footer__widget">
                  <h6><?= _FOOTER_MAIN4 ?></h6>
                  <ul>
                      <li><a href="https://www.gofx.com/trading-platform/" target="_blank"><?= _FOOTER_MAIN4_1 ?></a></li>
                      <li><a href="https://www.gofx.com/trading-platform/" target="_blank"><?= _FOOTER_MAIN4_2 ?></a></li>
                      <li><a href="https://www.gofx.com/trading-platform/" target="_blank"><?= _FOOTER_MAIN4_3 ?></a></li>
                  </ul>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-12">
              <div class="footer__widget">
                  <h6><?= _FOOTER_MAIN5 ?></h6>
                  <ul>
                      <li><a href="contact"><?= _FOOTER_MAIN5_1 ?></a></li>
                  </ul>
              </div>
            </div>
        </div>
    </div>
</footer>
<footer class="footer-award">
    <div class="container">
        <div class="row">
            <div class="col-lg col-md col-12">
                      <img src="images/footer_award/01.png" alt="" class="">
            </div>
            <div class="col-lg col-md col-12">
                      <img src="images/footer_award/02.png" alt="" class="footer-award-img-1">
            </div>
            <div class="col-lg col-md col-12">
                      <img src="images/footer_award/03.png" alt="" class="footer-award-img-2">
            </div>
            <div class="col-lg col-md col-12">
                      <img src="images/footer_award/04.png" alt="" class="footer-award-img-1">
            </div>
            <div class="col-lg col-md col-12">
                      <img src="images/footer_award/05.png" alt="" class="footer-award-img-3">
            </div>
        </div>
    </div>
</footer>
<footer class="footer-copyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-4 col-12">
                <div class="footer__copyright">
                    <div class="footer__copyright__text">
                      <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/logo/logo-gray.png" alt="" class="logo-gray">
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-12">
                <div class="footer__copyright">
                    <div class="footer__copyright__text footer_tel">
                      <img src="images/phone.png" alt="" class="">
                      02-026-6559
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-5 col-12">
                <div class="footer__copyright">
                    <div class="footer__copyright__text">
                        <a href="fb://www.facebook.com/GOFXthailand?_rdc=1&_rdr" target="_bank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/gofxthailand" target="_bank"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                        <a href="https://www.instagram.com/gofx_thailand/?hl=th" target="_bank"><i class="fab fa-line" aria-hidden="true"></i></a>
                        <a href="https://www.youtube.com/channel/UCrKNDDHpE1hR3ibZCo6gEmg/featured" target="_bank"><i class="fab fa-youtube" aria-hidden="true"></i></a>
                        <a href="https://lin.ee/jcDe4w5" target="_bank"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-ver height-60">
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->
<a id="button"></a>
<!-- Js Plugins -->
<!-- <script src="../js/jquery-3.3.1.min.js"></script> -->
<!-- <script src='../js/jquery.min.js'></script> -->
<script src="../js/script.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.nice-select.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/jquery.slicknav.js"></script>
<script src="../js/mixitup.min.js"></script>
<script src="../js/owl.carousel.min.js"></script>
<script src="../js/main.js"></script>
<script src="../js/jquery.scrolla.min.js"></script>
<script src="../js/example.min.js"></script>
<script type='text/javascript' src='https://service.force.com/embeddedservice/5.0/esw.min.js'></script>

<script>

// button back to top //
var btn = $('#button');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});

// hidden mobile navbar when scroll down //
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos || currentScrollPos <= 200) {
    document.getElementById("mobile_menu").style.top = "0";
  } else {
    document.getElementById("mobile_menu").style.top = "-80px";
  }
  prevScrollpos = currentScrollPos;
}

// mobile app when scroll down //
var prevScrollpos = window.pageYOffset;
  window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
    if (prevScrollpos > currentScrollPos || currentScrollPos >= 50) {
      document.getElementById("mobile_app_menu").style.paddingBottom  = "8px";
    } else {
      document.getElementById("mobile_app_menu").style.paddingBottom = "0px";
    }
    prevScrollpos = currentScrollPos;
  }

// index scroll timeline //
$(window).scroll(function() {
    var white = "<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073426_go_trader.gif";
    var green = "<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073414_advance_trader.gif";
    var black = "<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073359_expert_trader.gif";
    var red = "<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_065042_new_master_trader.gif";

    if (window.matchMedia("(min-width: 992px) and (max-width: 1199px)").matches) {
      $('#range').html(($(this).scrollTop() > 1700 && $(this).scrollTop() <= 2500) ? '2.8K - 105K': ($(this).scrollTop() > 2500 && $(this).scrollTop() <= 3100) ? '116K - 540K': ($(this).scrollTop() > 3100) ? '585K - 30M': '0 - 1.8K');
      $('#loyalty_status').html(($(this).scrollTop() > 1700 && $(this).scrollTop() <= 2500) ? 'Advance Trader': ($(this).scrollTop() > 2500 && $(this).scrollTop() <= 3100) ? 'Expert Trader': ($(this).scrollTop() > 3100) ? 'Master Trader': 'GO Trader');
      $('#lot_status').html(($(this).scrollTop() > 1700 && $(this).scrollTop() <= 2500) ? '1 Lot = 12 Point': ($(this).scrollTop() > 2500 && $(this).scrollTop() <= 3100) ? '1 Lot = 15 Point': ($(this).scrollTop() > 3100) ? '1 Lot = 20 Point': '1 Lot = 10 Point');

      var value = $(this).scrollTop();
     if (value > 1700 && value <= 2500){
        $("#loyalty_image").attr("src", green);
     }else if (value > 2500 && value <= 3100) {
       $("#loyalty_image").attr("src", black);
     }else if (value > 3100) {
       $("#loyalty_image").attr("src", red);
     }else{
       $("#loyalty_image").attr("src", white);
     }
   }else if (window.matchMedia("(min-width: 768px) and (max-width: 991px)").matches){
     $('#range').html(($(this).scrollTop() > 1550 && $(this).scrollTop() <= 2300) ? '2.8K - 105K': ($(this).scrollTop() > 2300 && $(this).scrollTop() <= 3050) ? '116K - 540K': ($(this).scrollTop() > 3050) ? '585K - 30M': '0 - 1.8K');
     $('#loyalty_status').html(($(this).scrollTop() > 1550 && $(this).scrollTop() <= 2300) ? 'Advance Trader': ($(this).scrollTop() > 2300 && $(this).scrollTop() <= 3050) ? 'Expert Trader': ($(this).scrollTop() > 3050) ? 'Master Trader': 'GO Trader');
     $('#lot_status').html(($(this).scrollTop() > 1550 && $(this).scrollTop() <= 2300) ? '1 Lot = 12 Point': ($(this).scrollTop() > 2300 && $(this).scrollTop() <= 3050) ? '1 Lot = 15 Point': ($(this).scrollTop() > 3050) ? '1 Lot = 20 Point': '1 Lot = 10 Point');

     var value = $(this).scrollTop();
    if (value > 1550 && value <= 2300){
       $("#loyalty_image").attr("src", green);
    }else if (value > 2300 && value <= 3050) {
      $("#loyalty_image").attr("src", black);
    }else if (value > 3050) {
      $("#loyalty_image").attr("src", red);
    }else{
      $("#loyalty_image").attr("src", white);
    }
   }else{
     $('#range').html(($(this).scrollTop() > 1500 && $(this).scrollTop() <= 2150) ? '2.8K - 105K': ($(this).scrollTop() > 2150 && $(this).scrollTop() <= 2750) ? '116K - 540K': ($(this).scrollTop() > 2750) ? '585K - 30M': '0 - 1.8K');
     $('#loyalty_status').html(($(this).scrollTop() > 1500 && $(this).scrollTop() <= 2150) ? 'Advance Trader': ($(this).scrollTop() > 2150 && $(this).scrollTop() <= 2750) ? 'Expert Trader': ($(this).scrollTop() > 2750) ? 'Master Trader': 'GO Trader');
     $('#lot_status').html(($(this).scrollTop() > 1500 && $(this).scrollTop() <= 2150) ? '1 Lot = 12 Point': ($(this).scrollTop() > 2150 && $(this).scrollTop() <= 2750) ? '1 Lot = 15 Point': ($(this).scrollTop() > 2750) ? '1 Lot = 20 Point': '1 Lot = 10 Point');

     var value = $(this).scrollTop();
    if (value > 1500 && value <= 2150){
       $("#loyalty_image").attr("src", green);
    }else if (value > 2150 && value <= 2750) {
      $("#loyalty_image").attr("src", black);
    }else if (value > 2750) {
      $("#loyalty_image").attr("src", red);
    }else{
      $("#loyalty_image").attr("src", white);
    }
   }
}).scroll();

// salesforce live chat //
$("#btn-livechat").click(function(){
    $('.helpButtonEnabled').trigger("click");
});


var initESW = function(gslbBaseURL) {
  embedded_svc.settings.displayHelpButton = true; //Or false
  embedded_svc.settings.language = ''; //For example, enter 'en' or 'en-US'

  embedded_svc.settings.defaultMinimizedText = "Support 24 hr."; //(Defaults to Chat with an Expert)
  embedded_svc.settings.disabledMinimizedText = "Support 24 hr."; //(Defaults to Agent Offline)

  embedded_svc.settings.offlineSupportMinimizedText = "Support 24 hr."; //(Defaults to Contact Us)

  embedded_svc.settings.enabledFeatures = ['LiveAgent'];
  embedded_svc.settings.entryFeature = 'LiveAgent';
  embedded_svc.init(
   'https://gofx.my.salesforce.com',
   'https://gofx.secure.force.com/liveAgentSetupFlow',
   gslbBaseURL,
   '00D5g000003bpR6',
   'LOYALTY',
   {
    baseLiveAgentContentURL: 'https://c.la2-c1-ukb.salesforceliveagent.com/content',
    deploymentId: '5725g0000008Vnj',
    buttonId: '5735g0000008Wsb',
    baseLiveAgentURL: 'https://d.la2-c1-ukb.salesforceliveagent.com/chat',
    eswLiveAgentDevName: 'EmbeddedServiceLiveAgent_Parent04I5g0000000AH0EAM_1791b996eed',
    isOfflineSupportEnabled: true
   }
  );
};

if (!window.embedded_svc) {
 var s = document.createElement('script');
 s.setAttribute('src', 'https://gofx.my.salesforce.com/embeddedservice/5.0/esw.min.js');
 s.onload = function() {
  initESW(null);
 };
 document.body.appendChild(s);
} else {
 initESW('https://service.force.com');
}

/* GOFX */
$(document).ready(function() {
  if ($('.dropdown-menu a.dropdown-toggle').length) {

    $(".dropdown-menu a.dropdown-toggle").on("click", function(e) {
        if (
            !$(this)
            .next()
            .hasClass("show")
        ) {
            $(this)
                .parents(".dropdown-menu")
                .first()
                .find(".show")
                .removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass("show");

        $(this)
            .parents("li.nav-item.dropdown.show")
            .on("hidden.bs.dropdown", function(e) {
                $(".dropdown-submenu .show").removeClass("show");
            });

        return false;
    });

    }
    // accordion js

    if ($('.collapse').length) {

        $('.collapse').on('shown.bs.collapse', function() {
            $(this).parent().find(".fa-plus-circle").removeClass("fa-plus-circle").addClass("fa-minus-circle");
        }).on('hidden.bs.collapse', function() {
            $(this).parent().find(".fa-minus-circle").removeClass("fa-minus-circle").addClass("fa-plus-circle");
        });

        $('.card-header a').click(function() {
            $('.card-header').removeClass('active');

            //If the panel was open and would be closed by this click, do not active it
            if (!$(this).closest('.card').find('.collapse').hasClass('in'))
                $(this).parents('.card-header').addClass('active');
        });

    }

    // Slider Ranger

    if ($('#slider-range-min , #slider-range-max').length) {
        $(function() {
            $("#slider-range-min").slider({
                range: "min",
                value: 3000,
                min: 1000,
                max: 5000,
                slide: function(event, ui) {
                    $("#amount").val("$" + ui.value);
                }
            });
            $("#amount").val("$" + $("#slider-range-min").slider("value"));
        });
        $(function() {
            $("#slider-range-max").slider({
                range: "min",
                min: 1,
                max: 10,
                value: 2,

                slide: function(event, ui) {
                    $("#j").val(ui.value);
                }
            });
            $("#j").val($("#slider-range-max").slider("value"));
        });
    }





    // header collapse

    if ($('.header-transparent').length) {
        $(window).scroll(function() {
            if ($(".header-transparent").offset().top > 50) {
                $(".navbar-fixed-top").addClass("top-nav-collapse");
            } else {
                $(".navbar-fixed-top").removeClass("top-nav-collapse");
            }
        });
    }



    // counter
    if ($('.counter').length) {
        $('.counter').each(function() {
            var $this = $(this),
                countTo = $this.attr('data-count');

            $({ countNum: $this.text() }).animate({
                    countNum: countTo
                },

                {
                    duration: 10000,
                    easing: 'linear',
                    step: function() {
                        $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                        $this.text(this.countNum);
                        //alert('finished');
                    }

                });
        });

    }

    // post fallery

    if ($('#post-gallery , .slider , .service ').length) {

        $("#post-gallery").owlCarousel({

            navigation: false, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            pagination: false,
            autoPlay: true

        });


        $(".slider").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 3000,
            paginationSpeed: 400,
            singleItem: true,
            pagination: true,
            autoPlay: true,
            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            addClassActive: true,

        });

        $(".service").owlCarousel({

            autoPlay: 3000, //Set AutoPlay to 3 seconds
            navigation: true, // Show next and prev buttons
            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            items: 3,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],
            pagination: false

        });





    }
});

$(document).ready(function () {
      $('#s-icons').click(function () {

          $('.navbar-nav-icon').toggleClass("show");
      });

      $(document).click(function (e) {

          var isChk = 0;
          var classList = e.target.className.split(/\s+/);

          for (var i = 0; i < classList.length; i++) {
              if (classList[i] === 'btn-social' || classList[i] === 'fa-ellipsis-h') {
                  isChk++;
              }
          }

          if(isChk === 0){
              if($('#s-icons ul').hasClass('show')){
                  $('.navbar-nav-icon').toggleClass("show");
              }

          }


      });


      $(".account-type-column a.btn").on({
          mouseenter: function () {

              $(this).parent().find('.block.account-type').addClass('on-hover');
          },
          mouseleave: function () {
              $(this).parent().find('.block.account-type').removeClass('on-hover');
          }
      });

      $("#navbarMenu.collapse").on('show.bs.collapse', function(e) {
          $('.sticky-top').addClass('on-collapse');
          $('body').addClass('shownavbarMenu');
      })

      $('#navbarMenu.collapse').on('hidden.bs.collapse', function () {
          $('body').removeClass('shownavbarMenu');
      });

  });
  (function() {
var phplive_e_1594312841 = document.createElement("script") ;
phplive_e_1594312841.type = "text/javascript" ;
phplive_e_1594312841.async = true ;
phplive_e_1594312841.src = "https://livechat.gofx.com/js/phplive_v2.js.php?v=0%7C1594312841%7C0%7C&" ;
document.getElementById("phplive_btn_1594312841").appendChild( phplive_e_1594312841 ) ;
if ( [].filter ) { document.getElementById("phplive_btn_1594312841").addEventListener( "click", function(){ phplive_launch_chat_0() } ) ; } else { document.getElementById("phplive_btn_1594312841").attachEvent( "onclick", function(){ phplive_launch_chat_0() } ) ; }
})() ;
</script>

</body>
</html>
