<?php $page = 'contact'; include('template/header_temp.php'); ?>
<? (isset($_SESSION['lang']) ? $lang = $_SESSION['lang'] : $lang = 'th'); ?>
<div class="container mgtb-3">
  <div class="row contact-content">
    <div class="col-12">
      <h3 class="history-title"><?= _CONTACT_HEAD ?></h3>
    </div>
  </div>

  <div class="container mgt-3">
    <div class="row">
      <div class="col-lg-3 col-md-6 col-12 contact-qrcode">
        <img src="images/qrcode.png" class="contact-qrcode">
        <img src="images/logo-line.png" class="contact-line">
      </div>
      <div class="col-lg-3 col-md-6 col-12 tb-ver">
        <div class="row contact-social">
          <div class="col-12">
            <img src="images/tel.png">
            <a href="tel:02-026-6559"><?= _CONTACT_SOCIAL_1 ?></a>
          </div>
          <div class="col-12">
            <img src="images/line.png">
            <a href="https://lin.ee/jcDe4w5"><?= _CONTACT_SOCIAL_2 ?></a>
          </div>
          <div class="col-12">
            <img src="images/mail.png">
            <a href="mailto:support@gofx.com"><?= _CONTACT_SOCIAL_3 ?></a>
          </div>
          <div class="col-12">
            <img src="images/fb.png">
            <a href="https://web.facebook.com/GOFXthailand" target="_blank"><?= _CONTACT_SOCIAL_4 ?></a>
          </div>
        </div>
      </div>
      <div class="col-lg-9 col-md-12 col-12 contact-title">
        <h3><?= _CONTACT_HEADER_TITLE ?></h3>
        <p><span><?= _CONTACT_HEADER_TEXT1 ?></span> <?= _CONTACT_HEADER_TEXT2 ?></p>
        <div class="row text-center contact-social tab-var-none">
          <div class="col-lg-3 col-md-3 col-12">
            <img src="images/tel.png">
            <a href="tel:02-026-6559"><?= _CONTACT_SOCIAL_1 ?></a>
          </div>
          <div class="col-lg-2 col-md-2 col-12">
            <img src="images/line.png">
            <a href="https://lin.ee/jcDe4w5"><?= _CONTACT_SOCIAL_2 ?></a>
          </div>
          <div class="col-lg-4 col-md-4 col-12">
            <img src="images/mail.png">
            <a href="mailto:support@gofx.com"><?= _CONTACT_SOCIAL_3 ?></a>
          </div>
          <div class="col-lg-3 col-md-3 col-12">
            <img src="images/fb.png">
            <a href="https://web.facebook.com/GOFXthailand" target="_blank"><?= _CONTACT_SOCIAL_4 ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fulid contact-footer">
  <div class="container mgt-3 contact-footer-content">
    <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/logo/logo.png" class="contact-footer-img">
    <p><?= _CONTACT_FOOTER_TEXT1 ?></p>
    <p><?= _CONTACT_FOOTER_TEXT2 ?></p>
    <p><?= _CONTACT_FOOTER_TEXT3 ?></p>
    <p class="contact-text-warning"><span class="contact-text-warning-red"><?= _CONTACT_FOOTER_TEXT4 ?></span></br>
    <span><?= $lang == 'en' ? ' ' : _CONTACT_FOOTER_TEXT5 ?></span></p>
  </div>
</div>
<?php include('template/footer_temp.php') ?>
<script>
  $(document).ready(function(){
    localStorage.removeItem('id_active');
  });
</script>
