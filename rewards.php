<?php $page = 'rewards'; include('template/header_temp.php'); ?>
<? (isset($_SESSION['lang']) ? $lang = $_SESSION['lang'] : $lang = 'th'); ?>
<script>
var url = window.location.search;
if(url.indexOf('?level=advance') !== -1){
localStorage.setItem('id_active', 'advance');
document.cookie = "id_active_1=advance";
}
</script>
<style>
.owl-carousel:not(.owl-loaded){
    opacity: 0;
    visibility:hidden;
    height:0;
}
</style>
<div class="container reward tab-var-none mb-ver-none mb-lan-none">
  <!-- GO-Trader -->
  <div class="item col-12 col-sm-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading reward-head-title">
          <div class="row">
            <div class="col-2 mb-ver-none">
              <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073426_go_trader.gif" class="img-fulid">
            </div>
            <div class="col-10 mb-ver-none">
              <h3><?= _REWARDTIMELINE_TEXT_1 ?></h3>
              <p><?= _REWARDTIMELINE_Go ?></p>
            </div>
            <div class="col-12">
              <section class="timeline">
                <ul>
                    <li class="arrow passline"></li>
                    <li class="none passline"><div class="content text-center mgb-5"></div></li>
                  <?php
                    $CntGOTrader = new Reward();
                    $objCntGOTrader = $CntGOTrader->fncCntGOTrader($email);
                    $cnt = $objCntGOTrader['cnt'];
                    $GOTrader = new Reward();
                    $objGOTrader = $GOTrader->fncGOTrader($email,$lang);
                    $numResult = mysqli_num_rows($objGOTrader);
                    $i = 1;
                    while( $row = mysqli_fetch_array($objGOTrader) ) {
                      $reward_id = $row["id"];

                      switch ($row["reward_status"]) {
                        case 1:
                          $class = 'pass';
                          break;

                        default:
                          $class = 'lock';
                          break;
                      }
                      if($i < $cnt){
                        $class = 'pass passline';
                      }
                      if($i == $numResult){
                        echo '<li class="'.$class.' border-none">';
                      }else{
                        echo '<li class="'.$class.'">';
                      }
                  ?>
                      <div class="content text-center mgb-5 animate" data-animate="zoomIn" >
                        <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= "$row[rewards_level_image]"; ?>" class="img-fulid">
                        <?php
                          $GOTraderHistory = new Reward();
                          $objGOTraderHistory = $GOTraderHistory->fncGOTraderHistory($email);
                          foreach($objGOTraderHistory as $item){
                            $reward_items_id = $item['id'];
                            if($reward_items_id == $reward_id){
                              echo '<img src="images/receive.png" class="img-fulid receive-reward">';
                            }
                          }
                        ?>
                        <div class="mgt-1">
                          <h4><?= "$row[rewards_level_title]"; ?></h4>
                          <p class="reward-short-desc"><?= $row['rewards_level_short_description']; ?></p>
                          <img src="images/point.png" class="img-fulid reward-point-img"><span class="reward-text-point"><?= number_format($row['rewards_level_point']),' ', _REWARD_TEXT_3 ?></span>
                          <button class="btn btn-danger btn-lg text-center timeline-button" onclick="RewardDetail('<?= "$row[id]" ?>','go')"><?= _REWARDTIMELINE_BUTTON ?></button>
                        </div>
                      </div>
                  </li>
                <?php $i++; } ?>
                </ul>
              </section>
            </div>
          </div>
        </div>
      </div>
  </div>
  <!-- Advance-Trader -->
  <div class="item col-12 col-sm-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading reward-head-title">
          <div class="row">
            <div class="col-2 mb-ver-none">
              <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073414_advance_trader.gif" class="img-fulid">
            </div>
            <div class="col mb-ver-none">
              <h3><?= _REWARDTIMELINE_TEXT_2 ?></h3>
              <p><?= _REWARDTIMELINE_Advance ?></p>
            </div>
            <div class="col-12">
              <section class="timeline">
                <ul>
                    <li class="arrow passline"></li>
                    <li class="none passline"><div class="content text-center mgb-5"></div></li>
                  <?php
                    $CntAdvanceTrader = new Reward();
                    $objCntAdvanceTrader = $CntAdvanceTrader->fncCntAdvanceTrader($email);
                    $cnt = $objCntAdvanceTrader['cnt'];

                    $AdvanceTrader = new Reward();
                    $objAdvanceTrader = $AdvanceTrader->fncAdvanceTrader($email,$lang);
                    $numResult = mysqli_num_rows($objAdvanceTrader);
                    $i = 1;
                    while( $row = mysqli_fetch_array($objAdvanceTrader) ) {
                      $reward_id = $row["id"];
                      switch ($row["reward_status"]) {
                        case 1:
                          $class = 'pass';
                          break;

                        default:
                          $class = 'lock';
                          break;
                      }
                      if($i < $cnt){
                        $class = 'pass passline';
                      }
                      if($i == $numResult){
                        echo '<li class="'.$class.' border-none">';
                      }else{
                        echo '<li class="'.$class.'">';
                      }
                  ?>
                      <div class="content text-center mgb-5 animate" data-animate="zoomIn">
                        <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= "$row[rewards_level_image]"; ?>" class="img-fulid">
                        <?php
                          $AdvanceTraderHistory = new Reward();
                          $objAdvanceTraderHistory = $AdvanceTraderHistory->fncAdvanceTradeHistory($email);
                          foreach($objAdvanceTraderHistory as $item){
                            $reward_items_id = $item['id'];
                            if($reward_items_id == $reward_id){
                              echo '<img src="images/receive.png" class="img-fulid receive-reward">';
                            }
                          }
                        ?>
                        <div class="mgt-1">
                          <h4><?= "$row[rewards_level_title]"; ?></h4>
                          <p class="reward-short-desc"><?= "$row[rewards_level_short_description]"; ?></p>
                          <img src="images/point.png" class="img-fulid reward-point-img"><span class="reward-text-point"><?= number_format($row['rewards_level_point']),' ', _REWARD_TEXT_3 ?></span>
                          <button class="btn btn-danger btn-lg text-center timeline-button" onclick="RewardDetail('<?= "$row[id]" ?>','advance')"><?= _REWARDTIMELINE_BUTTON ?></button>
                        </div>
                      </div>
                  </li>
                <?php $i++; } ?>
                </ul>
              </section>
            </div>
          </div>
        </div>
      </div>
  </div>
  <!-- Expert-Trader -->
  <div class="item col-12 col-sm-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading reward-head-title">
          <div class="row">
            <div class="col-2 mb-ver-none">
              <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073359_expert_trader.gif" class="img-fulid">
            </div>
            <div class="col mb-ver-none">
              <h3><?= _REWARDTIMELINE_TEXT_3 ?></h3>
              <p><?= _REWARDTIMELINE_Expert ?></p>
            </div>
            <div class="col-12">
              <section class="timeline">
                <ul>
                    <li class="arrow passline"></li>
                    <li class="none passline"><div class="content text-center mgb-5"></div></li>
                  <?php
                    $CntExpertTrader = new Reward();
                    $objCntExpertTrader = $CntExpertTrader->fncCntExpertTrader($email);
                    $cnt = $objCntExpertTrader['cnt'];

                    $ExpertTrader = new Reward();
                    $objExpertTrader = $ExpertTrader->fncExpertTrader($email,$lang);
                    $numResult = mysqli_num_rows($objExpertTrader);
                    $i = 1;
                    while( $row = mysqli_fetch_array($objExpertTrader) ) {
                      $reward_id = $row["id"];
                      switch ($row["reward_status"]) {
                        case 1:
                          $class = 'pass';
                          break;

                        default:
                          $class = 'lock';
                          break;
                      }
                      if($i < $cnt){
                        $class = 'pass passline';
                      }
                      if($i == $numResult){
                        echo '<li class="'.$class.' border-none">';
                      }else{
                        echo '<li class="'.$class.'">';
                      }
                  ?>
                      <div class="content text-center mgb-5 animate" data-animate="zoomIn">
                        <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= "$row[rewards_level_image]"; ?>" class="img-fulid">
                        <?php
                          $ExpertTraderHistory = new Reward();
                          $objExpertTraderHistory = $ExpertTraderHistory->fncExpertTradeHistory($email);
                          foreach($objExpertTraderHistory as $item){
                            $reward_items_id = $item['id'];
                            if($reward_items_id == $reward_id){
                              echo '<img src="images/receive.png" class="img-fulid receive-reward">';
                            }
                          }
                        ?>
                        <div class="mgt-1">
                          <h4><?= "$row[rewards_level_title]"; ?></h4>
                          <p class="reward-short-desc"><?= "$row[rewards_level_short_description]"; ?></p>
                          <img src="images/point.png" class="img-fulid reward-point-img"><span class="reward-text-point"><?= number_format($row['rewards_level_point']),' ', _REWARD_TEXT_3 ?></span>
                          <button class="btn btn-danger btn-lg text-center timeline-button" onclick="RewardDetail('<?= "$row[id]" ?>','expert')"><?= _REWARDTIMELINE_BUTTON ?></button>
                        </div>
                      </div>
                  </li>
                <?php $i++; } ?>
                </ul>
              </section>
            </div>
          </div>
        </div>
      </div>
  </div>
  <!-- Master-Trader -->
  <div class="item col-12 col-sm-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading reward-head-title">
          <div class="row">
            <div class="col-2 mb-ver-none">
              <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_065042_new_master_trader.gif" class="img-fulid">
            </div>
            <div class="col mb-ver-none">
              <h3><?= _REWARDTIMELINE_TEXT_4 ?></h3>
              <p><?= _REWARDTIMELINE_Master ?></p>
            </div>
            <div class="row justify-content-center">
            <div class="col-lg-12" id="master_trader_desktop">
              <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                <li class="nav-item col-4">
                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">
                    <img src="images/point.png" class="img-fulid reward-point-gored-img"><?= _REWARD_RANK_4_1 ?>
                  </a>
                </li>
                <li class="nav-item col-4">
                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">
                    <img src="images/point.png" class="img-fulid reward-point-gored-img"><?= _REWARD_RANK_4_2 ?>
                  </a>
                </li>
              </ul>
              <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                  <div class="col-12">
                    <section class="timeline">
                      <ul>
                          <li class="arrow passline"></li>
                          <li class="none passline"><div class="content text-center mgb-5"></div></li>
                        <?php
                          $CntMasterTrader1 = new Reward();
                          $objCntMasterTrader1 = $CntMasterTrader1->fncCntMasterTrader1($email);
                          $cnt = $objCntMasterTrader1['cnt'];

                          $MasterTrader1 = new Reward();
                          $objMasterTrader1 = $MasterTrader1->fncMasterTrader1($email,$lang);
                          $numResult = mysqli_num_rows($objMasterTrader1);
                          $i = 1;
                          while( $row = mysqli_fetch_array($objMasterTrader1) ) {
                            $reward_id = $row["id"];
                            switch ($row["reward_status"]) {
                              case 1:
                                $class = 'pass';
                                break;

                              default:
                                $class = 'lock';
                                break;
                            }
                            if($i < $cnt){
                              $class = 'pass passline';
                            }
                            if($i == $numResult){
                              echo '<li class="'.$class.' border-none">';
                            }else{
                              echo '<li class="'.$class.'">';
                            }
                        ?>
                            <div class="content text-center mgb-5 animate" data-animate="zoomIn">
                              <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= "$row[rewards_level_image]"; ?>" class="img-fulid">
                              <?php
                                $MasterTraderHistory = new Reward();
                                $objMasterTraderHistory = $MasterTraderHistory->fncMasterTradeHistory($email);
                                foreach($objMasterTraderHistory as $item){
                                  $reward_items_id = $item['id'];
                                  if($reward_items_id == $reward_id){
                                    echo '<img src="images/receive.png" class="img-fulid receive-reward">';
                                  }
                                }
                              ?>
                              <div class="mgt-1">
                                <h4><?= "$row[rewards_level_title]"; ?></h4>
                                <p class="reward-short-desc"><?= "$row[rewards_level_short_description]"; ?></p>
                                <img src="images/point.png" class="img-fulid reward-point-img"><span class="reward-text-point"><?= number_format($row['rewards_level_point']),' ', _REWARD_TEXT_3 ?></span>
                                <button class="btn btn-danger btn-lg text-center timeline-button" onclick="RewardDetail('<?= "$row[id]" ?>','master')"><?= _REWARDTIMELINE_BUTTON ?></button>
                              </div>
                            </div>
                        </li>
                      <?php $i++; } ?>
                      </ul>
                    </section>
                  </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                  <div class="col-12">
                    <section class="timeline">
                      <ul>
                          <li class="arrow passline"></li>
                          <li class="none passline"><div class="content text-center mgb-5"></div></li>
                        <?php
                          $CntMasterTrader2 = new Reward();
                          $objCntMasterTrader2 = $CntMasterTrader2->fncCntMasterTrader2($email);
                          $cnt = $objCntMasterTrader2['cnt'];

                          $MasterTrader2 = new Reward();
                          $objMasterTrader2 = $MasterTrader1->fncMasterTrader2($email,$lang);
                          $numResult = mysqli_num_rows($objMasterTrader2);
                          $i = 1;
                          while( $row = mysqli_fetch_array($objMasterTrader2) ) {
                            $reward_id = $row["id"];
                            switch ($row["reward_status"]) {
                              case 1:
                                $class = 'pass';
                                break;

                              default:
                                $class = 'lock';
                                break;
                            }
                            if($i < $cnt){
                              $class = 'pass passline';
                            }
                            if($i == $numResult){
                              echo '<li class="'.$class.' border-none">';
                            }else{
                              echo '<li class="'.$class.'">';
                            }
                        ?>
                            <div class="content text-center mgb-5 animate" data-animate="zoomIn">
                              <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= "$row[rewards_level_image]"; ?>" class="img-fulid">
                              <?php
                                $MasterTraderHistory = new Reward();
                                $objMasterTraderHistory = $MasterTraderHistory->fncMasterTradeHistory($email);
                                foreach($objMasterTraderHistory as $item){
                                  $reward_items_id = $item['id'];
                                  if($reward_items_id == $reward_id){
                                    echo '<img src="images/receive.png" class="img-fulid receive-reward">';
                                  }
                                }
                              ?>
                              <div class="mgt-1">
                                <h4><?= "$row[rewards_level_title]"; ?></h4>
                                <p class="reward-short-desc"><?= "$row[rewards_level_short_description]"; ?></p>
                                <img src="images/point.png" class="img-fulid reward-point-img"><span class="reward-text-point"><?= number_format($row['rewards_level_point']),' ', _REWARD_TEXT_3 ?></span>
                                <button class="btn btn-danger btn-lg text-center timeline-button" onclick="RewardDetail('<?= "$row[id]" ?>','master')"><?= _REWARDTIMELINE_BUTTON ?></button>
                              </div>
                            </div>
                        </li>
                      <?php $i++; } ?>
                      </ul>
                    </section>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>

  </div>
</div>

<div class="container reward">
  <div class="row mt-0 tb-ver mb-ver mb-lan">
    <div class="col-12">

      <!-- start nav tab -->
      <div id="slide-input-shield" class="slider-shield owl-carousel owl-theme row m-0 mb-4 owl-loaded owl-drag">
        <div class="owl-stage-outer">
          <div class="owl-stage" id="slide-shield" style="transition: all 0s ease 0s; width: 3472px; transform: translate3d(-578px, 0px, 0px);">

            <?php
              if(isset($_COOKIE["id_active"])){
              $level = $_COOKIE["id_active"];
            }else{
              $level = '';
            }
              $shield_array = array();

              $go = '<div class="owl-item active center" id="go-item">
                <div class="item">
                  <label class="card-block card-shield p-0 h-100 " style="cursor: pointer;">
                    <input type="radio" name="input-shield" class="input-shield hide" data-shield-swift="" data-shield-name="go" data-shield-account-number="" data-shield-id="" id="go" value="go" required="">
                    <img class="img-fluid img-shield" src="'.$_ENV['APP_STORAGE'].'/images/status/20240109_073426_go_trader.gif">
                    <p class="reward-slide-text-sm" id="go-text-1">'._REWARD_DETAIL_TEXT_1.'</p>
                    <p class="reward-slide-text" id="go-text-2">'._REWARD_RANK_1.'</p>
                  </label>
                </div>
              </div>';

              $advance = '<div class="owl-item" id="advance-item">
                  <div class="item">
                    <label class="card-block card-shield p-0 h-100 " style="cursor: pointer;">
                      <input type="radio" name="input-shield" class="input-shield hide" data-shield-swift="" data-shield-name="advance" data-shield-account-number="" data-shield-id="" id="advance" value="advance" required="">
                      <img class="img-fluid img-shield" src="'.$_ENV['APP_STORAGE'].'/images/status/20240109_073414_advance_trader.gif">
                      <p class="reward-slide-text-sm" id="advance-text-1">'._REWARD_DETAIL_TEXT_1.'</p>
                      <p class="reward-slide-text" id="advance-text-2">'._REWARD_RANK_2.'</p>
                    </label>
                  </div>
                </div>';

              $expert = '<div class="owl-item" id="expert-item">
                <div class="item">
                  <label class="card-block card-shield p-0 h-100 " style="cursor: pointer;">
                    <input type="radio" name="input-shield" class="input-shield hide" data-shield-swift="" data-shield-name="expert" data-shield-account-number="" data-shield-id="" id="expert" value="expert" required="">
                    <img class="img-fluid img-shield" src="'.$_ENV['APP_STORAGE'].'/images/status/20240109_073359_expert_trader.gif">
                    <p class="reward-slide-text-sm" id="expert-text-1">'._REWARD_DETAIL_TEXT_1.'</p>
                    <p class="reward-slide-text" id="expert-text-2">'._REWARD_RANK_3.'</p>
                  </label>
                </div>
              </div>';

              $master = '<div class="owl-item" id="master-item">
                <div class="item">
                  <label class="card-block card-shield p-0 h-100 " style="cursor: pointer;">
                    <input type="radio" name="input-shield" class="input-shield hide" data-shield-swift="" data-shield-name="master" data-shield-account-number="" data-shield-id="" id="master" value="master" required="">
                    <img class="img-fluid img-shield" src="'.$_ENV['APP_STORAGE'].'/images/status/20240109_065042_new_master_trader.gif">
                    <p class="reward-slide-text-sm" id="master-text-1">'._REWARD_DETAIL_TEXT_1.'</p>
                    <p class="reward-slide-text" id="master-text-2">'._REWARD_RANK_4.'</p>
                  </label>
                </div>
              </div>';

              if($level != null){
                $array = ["go", "advance", "expert", "master"];
                $searchKey = array_keys($array, $level)[0];
                $i = 1;

                foreach ($array as $key => $value) {
                  $newKey = $key - $searchKey;
                  if ($searchKey == 1) $newKey = $newKey < 0 ? 3 : $newKey;
                  else $newKey = $newKey < 0 ? 3 - $i  : $newKey;

                  $newArray[$newKey] = $value;
                  $i--;
                }
                ksort($newArray);

                foreach($newArray as $item){
                  switch ($item) {
                    case 'go':
                      array_push($shield_array, $go);
                    break;

                    case 'advance':
                      array_push($shield_array, $advance);
                    break;

                    case 'expert':
                      array_push($shield_array, $expert);
                    break;

                    case 'master':
                      array_push($shield_array, $master);
                    break;

                    default:
                  }
                }
                foreach($shield_array as $items){
                  echo $items;
                }
              }else{
                array_push($shield_array,$go,$advance,$expert,$master);
                foreach($shield_array as $items){
                  echo $items;
                }
              }
            ?>
          </div>
        </div>

        <div class="owl-nav">
          <button type="button" role="presentation" class="owl-prev">
            <span aria-label="Previous"></span>
          </button>
          <button type="button" role="presentation" class="owl-next">
            <span aria-label="Next"></span>
          </button>
        </div>
      </div>
      <!-- end nav tab -->
    </div>
  </div>
  <div class="row go_trader_tab tb-ver mb-ver mb-lan">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="panel-heading reward-head-title">
        <div class="row">
          <div class="col-2 mb-ver-none">
            <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073426_go_trader.gif" class="img-fulid">
          </div>
          <div class="col-10 mb-ver-none">
            <h3><?= _REWARDTIMELINE_TEXT_1 ?></h3>
            <p><?= _REWARDTIMELINE_Go ?></p>
          </div>
          <div class="col-12">
            <section class="timeline">
              <ul>
                  <li class="arrow passline"></li>
                  <li class="none passline"><div class="content text-center mgb-5"></div></li>
                <?php
                  $CntGOTrader = new Reward();
                  $objCntGOTrader = $CntGOTrader->fncCntGOTrader($email);
                  $cnt = $objCntGOTrader['cnt'];
                  $GOTrader = new Reward();
                  $objGOTrader = $GOTrader->fncGOTrader($email,$lang);
                  $numResult = mysqli_num_rows($objGOTrader);
                  $i = 1;
                  while( $row = mysqli_fetch_array($objGOTrader) ) {
                    $reward_id = $row["id"];

                    switch ($row["reward_status"]) {
                      case 1:
                        $class = 'pass';
                        break;

                      default:
                        $class = 'lock';
                        break;
                    }
                    if($i < $cnt){
                      $class = 'pass passline';
                    }
                    if($i == $numResult){
                      echo '<li class="'.$class.' border-none">';
                    }else{
                      echo '<li class="'.$class.'">';
                    }
                ?>
                    <div class="content text-center mgb-5 animate" data-animate="zoomIn" >
                      <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= "$row[rewards_level_image]"; ?>" class="img-fulid">
                      <?php
                        $GOTraderHistory = new Reward();
                        $objGOTraderHistory = $GOTraderHistory->fncGOTraderHistory($email);
                        foreach($objGOTraderHistory as $item){
                          $reward_items_id = $item['id'];
                          if($reward_items_id == $reward_id){
                            echo '<img src="images/receive.png" class="img-fulid receive-reward">';
                          }
                        }
                      ?>
                      <div class="mgt-1">
                        <h4><?= "$row[rewards_level_title]"; ?></h4>
                        <p class="reward-short-desc"><?= "$row[rewards_level_short_description]"; ?></p>
                        <img src="images/point.png" class="img-fulid reward-point-img"><span class="reward-text-point"><?= number_format($row['rewards_level_point']),' ', _REWARD_TEXT_3 ?></span>
                        <button class="btn btn-danger btn-lg text-center timeline-button" onclick="RewardDetail('<?= "$row[id]" ?>','go')"><?= _REWARDTIMELINE_BUTTON ?></button>
                      </div>
                    </div>
                </li>
              <?php $i++; } ?>
              </ul>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Go trade end -->

  <div class="row advance_trader_tab hide tb-ver mb-ver mb-lan">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="panel-heading reward-head-title">
        <div class="row">
          <div class="col-2 mb-ver-none">
            <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073414_advance_trader.gif" class="img-fulid">
          </div>
          <div class="col mb-ver-none">
            <h3><?= _REWARDTIMELINE_TEXT_2 ?></h3>
            <p><?= _REWARDTIMELINE_Advance ?></p>
          </div>
          <div class="col-12">
            <section class="timeline">
              <ul>
                  <li class="arrow passline"></li>
                  <li class="none passline"><div class="content text-center mgb-5"></div></li>
                <?php
                  $CntAdvanceTrader = new Reward();
                  $objCntAdvanceTrader = $CntAdvanceTrader->fncCntAdvanceTrader($email);
                  $cnt = $objCntAdvanceTrader['cnt'];

                  $AdvanceTrader = new Reward();
                  $objAdvanceTrader = $AdvanceTrader->fncAdvanceTrader($email,$lang);
                  $numResult = mysqli_num_rows($objAdvanceTrader);
                  $i = 1;
                  while( $row = mysqli_fetch_array($objAdvanceTrader) ) {
                    $reward_id = $row["id"];
                    switch ($row["reward_status"]) {
                      case 1:
                        $class = 'pass';
                        break;

                      default:
                        $class = 'lock';
                        break;
                    }
                    if($i < $cnt){
                      $class = 'pass passline';
                    }
                    if($i == $numResult){
                      echo '<li class="'.$class.' border-none">';
                    }else{
                      echo '<li class="'.$class.'">';
                    }
                ?>
                    <div class="content text-center mgb-5 animate" data-animate="zoomIn">
                      <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= "$row[rewards_level_image]"; ?>" class="img-fulid">
                      <?php
                        $AdvanceTraderHistory = new Reward();
                        $objAdvanceTraderHistory = $AdvanceTraderHistory->fncAdvanceTradeHistory($email);
                        foreach($objAdvanceTraderHistory as $item){
                          $reward_items_id = $item['id'];
                          if($reward_items_id == $reward_id){
                            echo '<img src="images/receive.png" class="img-fulid receive-reward">';
                          }
                        }
                      ?>
                      <div class="mgt-1">
                        <h4><?= "$row[rewards_level_title]"; ?></h4>
                        <p class="reward-short-desc"><?= "$row[rewards_level_short_description]"; ?></p>
                        <img src="images/point.png" class="img-fulid reward-point-img"><span class="reward-text-point"><?= number_format($row['rewards_level_point']),' ', _REWARD_TEXT_3 ?></span>
                        <button class="btn btn-danger btn-lg text-center timeline-button" onclick="RewardDetail('<?= "$row[id]" ?>','advance')"><?= _REWARDTIMELINE_BUTTON ?></button>
                      </div>
                    </div>
                </li>
              <?php $i++; } ?>
              </ul>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Advance trade end -->

  <div class="row expert_trader_tab hide tb-ver mb-ver mb-lan">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="panel-heading reward-head-title">
        <div class="row">
          <div class="col-2 mb-ver-none">
            <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_073359_expert_trader.gif" class="img-fulid">
          </div>
          <div class="col mb-ver-none">
            <h3><?= _REWARDTIMELINE_TEXT_3 ?></h3>
            <p><?= _REWARDTIMELINE_Expert ?></p>
          </div>
          <div class="col-12">
            <section class="timeline">
              <ul>
                  <li class="arrow passline"></li>
                  <li class="none passline"><div class="content text-center mgb-5"></div></li>
                <?php
                  $CntExpertTrader = new Reward();
                  $objCntExpertTrader = $CntExpertTrader->fncCntExpertTrader($email);
                  $cnt = $objCntExpertTrader['cnt'];

                  $ExpertTrader = new Reward();
                  $objExpertTrader = $ExpertTrader->fncExpertTrader($email,$lang);
                  $numResult = mysqli_num_rows($objExpertTrader);
                  $i = 1;
                  while( $row = mysqli_fetch_array($objExpertTrader) ) {
                    $reward_id = $row["id"];
                    switch ($row["reward_status"]) {
                      case 1:
                        $class = 'pass';
                        break;

                      default:
                        $class = 'lock';
                        break;
                    }
                    if($i < $cnt){
                      $class = 'pass passline';
                    }
                    if($i == $numResult){
                      echo '<li class="'.$class.' border-none">';
                    }else{
                      echo '<li class="'.$class.'">';
                    }
                ?>
                    <div class="content text-center mgb-5 animate" data-animate="zoomIn">
                      <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= "$row[rewards_level_image]"; ?>" class="img-fulid">
                      <?php
                        $ExpertTraderHistory = new Reward();
                        $objExpertTraderHistory = $ExpertTraderHistory->fncExpertTradeHistory($email);
                        foreach($objExpertTraderHistory as $item){
                          $reward_items_id = $item['id'];
                          if($reward_items_id == $reward_id){
                            echo '<img src="images/receive.png" class="img-fulid receive-reward">';
                          }
                        }
                      ?>
                      <div class="mgt-1">
                        <h4><?= "$row[rewards_level_title]"; ?></h4>
                        <p class="reward-short-desc"><?= "$row[rewards_level_short_description]"; ?></p>
                        <img src="images/point.png" class="img-fulid reward-point-img"><span class="reward-text-point"><?= number_format($row['rewards_level_point']),' ', _REWARD_TEXT_3 ?></span>
                        <button class="btn btn-danger btn-lg text-center timeline-button" onclick="RewardDetail('<?= "$row[id]" ?>','expert')"><?= _REWARDTIMELINE_BUTTON ?></button>
                      </div>
                    </div>
                </li>
              <?php $i++; } ?>
              </ul>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Expert trade end -->

  <div class="row master_trader_tab hide tb-ver mb-ver mb-lan" id="master_trader_tab">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="panel-heading reward-head-title">
        <div class="row">
          <div class="col-2 mb-ver-none">
            <img src="<?php echo $_ENV['APP_STORAGE']; ?>/images/status/20240109_065042_new_master_trader.gif" class="img-fulid">
          </div>
          <div class="col mb-ver-none">
            <h3><?= _REWARDTIMELINE_TEXT_4 ?></h3>
            <p><?= _REWARDTIMELINE_Master ?></p>
          </div>
          <div class="col-12">
            <ul class="nav nav-pills mb-3" id="MasterTraderTab" role="tablist">
              <li class="nav-item">
                <button class="nav-link active" id="master_1-tab" data-toggle="tab" href="#master_1" role="tab" aria-controls="master_1" aria-selected="true">
                  <img src="images/point.png" class="img-fulid reward-point-gored-img"><?= _REWARD_RANK_4_1 ?>
                </button>
              </li>
              <li class="nav-item">
                <button class="nav-link" id="master_2-tab" data-toggle="tab" href="#master_2" role="tab" aria-controls="master_2" aria-selected="false">
                  <img src="images/point.png" class="img-fulid reward-point-gored-img"><?= _REWARD_RANK_4_2 ?>
                </button>
              </li>
            </ul>
            <div class="tab-content mgt-3" id="MasterTraderTabContent">
              <div class="tab-pane fade show active" id="master_1" role="tabpanel" aria-labelledby="master_1-tab">
                <section class="timeline">
                  <ul>
                      <li class="arrow passline"></li>
                      <li class="none passline"><div class="content text-center mgb-5"></div></li>
                    <?php
                      $CntMasterTrader1 = new Reward();
                      $objCntMasterTrader1 = $CntMasterTrader1->fncCntMasterTrader1($email);
                      $cnt = $objCntMasterTrader1['cnt'];

                      $MasterTrader1 = new Reward();
                      $objMasterTrader1 = $MasterTrader1->fncMasterTrader1($email,$lang);
                      $numResult = mysqli_num_rows($objMasterTrader1);
                      $i = 1;
                      while( $row = mysqli_fetch_array($objMasterTrader1) ) {
                        $reward_id = $row["id"];
                        switch ($row["reward_status"]) {
                          case 1:
                            $class = 'pass';
                            break;

                          default:
                            $class = 'lock';
                            break;
                        }
                        if($i < $cnt){
                          $class = 'pass passline';
                        }
                        if($i == $numResult){
                          echo '<li class="'.$class.' border-none">';
                        }else{
                          echo '<li class="'.$class.'">';
                        }
                    ?>
                        <div class="content text-center mgb-5 animate" data-animate="zoomIn">
                          <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= "$row[rewards_level_image]"; ?>" class="img-fulid">
                          <?php
                            $MasterTraderHistory = new Reward();
                            $objMasterTraderHistory = $MasterTraderHistory->fncMasterTradeHistory($email);
                            foreach($objMasterTraderHistory as $item){
                              $reward_items_id = $item['id'];
                              if($reward_items_id == $reward_id){
                                echo '<img src="images/receive.png" class="img-fulid receive-reward">';
                              }
                            }
                          ?>
                          <div class="mgt-1">
                            <h4><?= "$row[rewards_level_title]"; ?></h4>
                            <p class="reward-short-desc"><?= "$row[rewards_level_short_description]"; ?></p>
                            <img src="images/point.png" class="img-fulid reward-point-img"><span class="reward-text-point"><?= number_format($row['rewards_level_point']),' ', _REWARD_TEXT_3 ?></span>
                            <button class="btn btn-danger btn-lg text-center timeline-button" onclick="RewardDetail('<?= "$row[id]" ?>','master')"><?= _REWARDTIMELINE_BUTTON ?></button>
                          </div>
                        </div>
                    </li>
                  <?php $i++; } ?>
                  </ul>
                </section>
              </div>
              <div class="tab-pane fade" id="master_2" role="tabpanel" aria-labelledby="master_2-tab">
                <section class="timeline">
                  <ul>
                      <li class="arrow passline"></li>
                      <li class="none passline"><div class="content text-center mgb-5"></div></li>
                    <?php
                      $CntMasterTrader2 = new Reward();
                      $objCntMasterTrader2 = $CntMasterTrader2->fncCntMasterTrader2($email);
                      $cnt = $objCntMasterTrader2['cnt'];

                      $MasterTrader2 = new Reward();
                      $objMasterTrader2 = $MasterTrader1->fncMasterTrader2($email,$lang);
                      $numResult = mysqli_num_rows($objMasterTrader2);
                      $i = 1;
                      while( $row = mysqli_fetch_array($objMasterTrader2) ) {
                        $reward_id = $row["id"];
                        switch ($row["reward_status"]) {
                          case 1:
                            $class = 'pass';
                            break;

                          default:
                            $class = 'lock';
                            break;
                        }
                        if($i < $cnt){
                          $class = 'pass passline';
                        }
                        if($i == $numResult){
                          echo '<li class="'.$class.' border-none">';
                        }else{
                          echo '<li class="'.$class.'">';
                        }
                    ?>
                        <div class="content text-center mgb-5 animate" data-animate="zoomIn">
                          <img src="<?php echo $_ENV['APP_STORAGE']; ?>/<?= "$row[rewards_level_image]"; ?>" class="img-fulid">
                          <?php
                            $MasterTraderHistory = new Reward();
                            $objMasterTraderHistory = $MasterTraderHistory->fncMasterTradeHistory($email);
                            foreach($objMasterTraderHistory as $item){
                              $reward_items_id = $item['id'];
                              if($reward_items_id == $reward_id){
                                echo '<img src="images/receive.png" class="img-fulid receive-reward">';
                              }
                            }
                          ?>
                          <div class="mgt-1">
                            <h4><?= "$row[rewards_level_title]"; ?></h4>
                            <p class="reward-short-desc"><?= "$row[rewards_level_short_description]"; ?></p>
                            <img src="images/point.png" class="img-fulid reward-point-img"><span class="reward-text-point"><?= number_format($row['rewards_level_point']),' ', _REWARD_TEXT_3 ?></span>
                            <button class="btn btn-danger btn-lg text-center timeline-button" onclick="RewardDetail('<?= "$row[id]" ?>','master')"><?= _REWARDTIMELINE_BUTTON ?></button>
                          </div>
                        </div>
                    </li>
                  <?php $i++; } ?>
                  </ul>
                </section>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Master trade end -->
</div>
<!-- partial:index.partial.html -->
<?php  include('template/footer_temp.php') ?>
<script src="https://secure.gofx.com/forex_2.0.0_dataable_custom/themplate/dataable_custom/assets/plugins/owl-carousel/js/owl.carousel.min.js"></script>
  <script>


          $('.slider-shield').owlCarousel({
                  center: true,
                  loop: true,
                  margin:10,
                  nav:true,
                  dots: true,
                  responsive:{
                      0:{
                          items:1
                      },
                      600:{
                          items:3
                      },
                      1000:{
                          items:3
                      }
                  }
              })

              $('.owl-prev').click(function() {
                var current = $('.owl-item.active.center input').val();
                switch (current) {

                  case "go":
                      $('.go_trader_tab').removeClass('hide');
                      $('.advance_trader_tab').addClass('hide');
                      $('.expert_trader_tab').addClass('hide');
                      $('.master_trader_tab').addClass('hide');
                  break;

                  case "advance":
                      $('.go_trader_tab').addClass('hide');
                      $('.advance_trader_tab').removeClass('hide');
                      $('.expert_trader_tab').addClass('hide');
                      $('.master_trader_tab').addClass('hide');
                  break;

                  case "expert":
                      $('.go_trader_tab').addClass('hide');
                      $('.advance_trader_tab').addClass('hide');
                      $('.expert_trader_tab').removeClass('hide');
                      $('.master_trader_tab').addClass('hide');
                  break;

                  case "master":
                      $('.go_trader_tab').addClass('hide');
                      $('.advance_trader_tab').addClass('hide');
                      $('.expert_trader_tab').addClass('hide');
                      $('.master_trader_tab').removeClass('hide');
                    break;

                  default:
                    //the id is none of the above
                }

              })
              // Go to the previous item
              $('.owl-next').click(function() {
                var current = $('.owl-item.active.center input').val();
                switch (current) {

                  case "go":
                      $('.go_trader_tab').removeClass('hide');
                      $('.advance_trader_tab').addClass('hide');
                      $('.expert_trader_tab').addClass('hide');
                      $('.master_trader_tab').addClass('hide');

                  break;

                  case "advance":
                      $('.go_trader_tab').addClass('hide');
                      $('.advance_trader_tab').removeClass('hide');
                      $('.expert_trader_tab').addClass('hide');
                      $('.master_trader_tab').addClass('hide');
                  break;

                  case "expert":
                      $('.go_trader_tab').addClass('hide');
                      $('.advance_trader_tab').addClass('hide');
                      $('.expert_trader_tab').removeClass('hide');
                      $('.master_trader_tab').addClass('hide');
                  break;

                  case "master":
                      $('.go_trader_tab').addClass('hide');
                      $('.advance_trader_tab').addClass('hide');
                      $('.expert_trader_tab').addClass('hide');
                      $('.master_trader_tab').removeClass('hide');
                    break;

                  default:
                    //the id is none of the above
                }

              })
              $('#slide-input-shield').on('dragged.owl.carousel', function(e) {
                var current = $('.owl-item.active.center input').val();
                switch (current) {

                  case "go":
                      $('.go_trader_tab').removeClass('hide');
                      $('.advance_trader_tab').addClass('hide');
                      $('.expert_trader_tab').addClass('hide');
                      $('.master_trader_tab').addClass('hide');
                  break;

                  case "advance":
                      $('.go_trader_tab').addClass('hide');
                      $('.advance_trader_tab').removeClass('hide');
                      $('.expert_trader_tab').addClass('hide');
                      $('.master_trader_tab').addClass('hide');
                  break;

                  case "expert":
                      $('.go_trader_tab').addClass('hide');
                      $('.advance_trader_tab').addClass('hide');
                      $('.expert_trader_tab').removeClass('hide');
                      $('.master_trader_tab').addClass('hide');
                  break;

                  case "master":
                      $('.go_trader_tab').addClass('hide');
                      $('.advance_trader_tab').addClass('hide');
                      $('.expert_trader_tab').addClass('hide');
                      $('.master_trader_tab').removeClass('hide');
                    break;

                  default:
                    //the id is none of the above
                }

              })


          $(window).on('resize', function(){
          var windowWidth = jQuery(window).width();
        });

  </script>
<script>
  $(document).ready(function(){
    var active = localStorage.getItem('id_active');
    if(active !== null){
      $('#'+active).trigger("click");
      $('#mb_'+active).addClass('active');
      document.cookie = "id_active=";
      switch (active) {
        case "go":
            $('.go_trader_tab').removeClass('hide');
            $('.advance_trader_tab').addClass('hide');
            $('.expert_trader_tab').addClass('hide');
            $('.master_trader_tab').addClass('hide');

        break;

        case "advance":
            $('.go_trader_tab').addClass('hide');
            $('.advance_trader_tab').removeClass('hide');
            $('.expert_trader_tab').addClass('hide');
            $('.master_trader_tab').addClass('hide');


            var carousel = $("#slide");
            carousel.trigger('to.owl.carousel', [1, 500]);
        break;

        case "expert":
            $('.go_trader_tab').addClass('hide');
            $('.advance_trader_tab').addClass('hide');
            $('.expert_trader_tab').removeClass('hide');
            $('.master_trader_tab').addClass('hide');

        break;

        case "master":
            $('.go_trader_tab').addClass('hide');
            $('.advance_trader_tab').addClass('hide');
            $('.expert_trader_tab').addClass('hide');
            $('.master_trader_tab').removeClass('hide');

          break;

        default:
      }
      localStorage.removeItem('id_active');
    }else{
      $('#mb_go').addClass('active');
    }
  });

  function RewardDetail(level_id,id_active){
    document.cookie = "Level="+ level_id;
    localStorage.setItem('id_active', id_active);
    window.location = 'reward_detail';
	}
</script>
