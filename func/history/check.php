<?php

include_once __DIR__ . '/../Query.php';
include_once __DIR__ . '/../DotENV.php';

use DevCoder\DotEnv;
(new DotEnv(__DIR__ . '/../.env'))->load();

$conn = new Query();


$data = ['address'=>0,'history'=>1];
if ($_POST) {
  // Check Address
  $sql = "SELECT * FROM user_address WHERE user_id = '$_POST[user_id]'";
  $result = $conn->select_assoc($sql);
  if ($result) {
    $data['address'] = 1;
  }


  $sql = "SELECT * FROM history WHERE user_id = '$_POST[user_id]' AND reward_items_id = '$_POST[reward_items_id]' AND history_status != 4";
  $result = $conn->select_assoc($sql);
  if (!$result && $data['address'] == 1) {
    $item_id = $_POST['reward_items_id'];
    $name = $_POST['name'];
    $email =  $_POST['email'];
    $user_id = $_POST['user_id'];
    if(isset($_POST['size'])){
      unset($_POST['name'],$_POST['email']);
      $size = $_POST['size']; 
      $sql = "INSERT INTO special_item_history (user_id,reward_items_id,size) VALUES ('$user_id','$item_id','$size')";
      $conn->query_data($sql);
    }
   
    $sql = "SELECT * FROM history WHERE user_id = $_POST[user_id] AND reward_items_id = $_POST[reward_items_id]";
    $history = $conn->select_assoc($sql);
    if (!count($history)) {
      unset($_POST['name'], $_POST['email'],$_POST['size']);
      $sql = "SELECT * FROM history";
      $reward_items_id = $_POST['reward_items_id'];
      // $reward_items_name = $_POST['reward_items_name'];
      // $reward_items_image = $_POST['reward_items_image'];
      $history_status = 1;
      $sql = "INSERT INTO history (user_id,reward_items_id,history_status) VALUES ('$user_id', '$reward_items_id', '$history_status')";
      $conn->query_data($sql);

      $log['user_id'] = $_POST['user_id'];
      $sql = "SELECT *
              FROM history
              WHERE user_id = $_POST[user_id]
              ORDER BY id DESC";
      $result = $conn->select_assoc($sql);
      $_POST['id'] = $result['id'];
      $log['history_id'] = $_POST['id'];
      $_POST['history_status'] = 1;
      unset($_POST['user_id']);
      $log['value'] = json_encode($_POST, JSON_UNESCAPED_UNICODE);
      $log['type'] = "history";
      $fields = implode(',', array_keys($log));
      $values = "'" . implode("','", $log) . "'";
      $sql = "INSERT INTO `log` ($fields) VALUES ($values)";
      $conn->query_data($sql);

      $address = $conn->select_assoc("SELECT `address`, district, amphure, province, zipcode, tel FROM user_address WHERE user_id = " . $user_id);
      $address['history_id'] = $_POST['id'];
      $fields = implode(',', array_keys($address));
      $values = implode("','", $address);
      $values = "'" . $values . "'";
      $conn->query_data("INSERT INTO history_user_address ($fields) VALUES ($values)");
      unset($log);
    }
    else {
      $sql = "UPDATE history SET history_status = 1 WHERE user_id = $_POST[user_id] AND reward_items_id = $_POST[reward_items_id]";
      $conn->query_data($sql);

      $result = $conn->select_assoc("SELECT * FROM history WHERE user_id = $_POST[user_id] AND reward_items_id = $_POST[reward_items_id]");
      $value['id'] = $result['id'];
      $value['reward_items_id'] = $result['reward_items_id'];
      $value['history_status'] = $result['history_status'];
      $value = json_encode($value, JSON_UNESCAPED_UNICODE);
      $sql = "UPDATE `log` SET `value` = '$value' WHERE user_id = $_POST[user_id] AND `value` LIKE '%\"reward_items_id\":$_POST[reward_items_id]%'";
      $conn->query_data($sql);
    }

    $log['item_id'] = $item_id;
    $log['history_status'] = 1;
    $log['customer_id'] = $user_id;
    $field = implode(',', array_keys($log));
    $value = implode("','", $log);
    $value = "'" . $value . "'";
    $conn->query_data("INSERT INTO log_award_history ($field) VALUES ($value)");

    if ($_ENV['LINE_NOTIFICATION'] == "on") {
      $conn->LineNotify($item_id,$name,$email);
    }
    
    
    $data['history'] = 0;
  }
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);