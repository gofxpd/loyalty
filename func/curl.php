<?php

class Curl {

  protected $method;
  protected $param;

  private function api(string $url) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://servicefx.gofx.com/v1' . $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => $this->method,
      CURLOPT_POSTFIELDS => $this->param,
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $json = json_decode($response);
    if ($json->code == 200) return $json->result;
    else return $json->status_process;
  }

  public function post($url, $param = false) {
    $this->method = "POST";
    $this->param = $param;

    return $this->api($url);
  }

}