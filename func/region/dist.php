<?php

require_once __DIR__ . '/../Region.php';

$conn = new Region();


if ($_GET['id']) {
  $result = $conn->all("SELECT * FROM districts WHERE zip_code != 0 AND amphure_id = " . $_GET['id']);

  echo json_encode($result, JSON_UNESCAPED_UNICODE);
}