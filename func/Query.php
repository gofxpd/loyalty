<?php

include_once 'conn.php';

class Query extends DB_con {
  public function query_data($sql) {
    return mysqli_query($this->dbcon, $sql);
  }

  // ALL
  public function select_all($sql) {
    return mysqli_fetch_all($this->query_data($sql), MYSQLI_ASSOC);
  }

  // ASSOC
  public function select_assoc($sql) {
    return mysqli_fetch_assoc($this->query_data($sql));
  }

  private function historyStatus($status) {
    switch ($status) {
      case 1: return "Pending";
      case 2: return "In progress";
      case 3: return "Success";
    }
  }

  // LINE
  public function LineNotify($item_id,$name,$email) {
    $sql = "SELECT a.reward_items_title, b.reward_items_img, d.level_title, c.rewards_level_title, c.rewards_level_point, h.history_status
            FROM reward_items_detail_th a
            LEFT JOIN reward_items b ON b.id = a.reward_items_id
            LEFT JOIN rewards_level c ON b.reward_level_id = c.id
            LEFT JOIN level d ON c.level_id = d.id
            INNER JOIN history h ON h.reward_items_id = b.id
           WHERE a.reward_items_id = $item_id";
    $result = $this->select_assoc($sql);

    $reward_items_title = $result['reward_items_title'];
    $level_title = $result['level_title'];
    $reward_point = number_format($result['rewards_level_point']);


    $sql = "SELECT h.size
            FROM origin_lots o
            INNER JOIN special_item_history h ON h.user_id = o.id
            WHERE o.email = '$email' AND h.reward_items_id = $item_id";
    $result = $this->select_assoc($sql);

    $size = !empty($result['size']) && ($item_id == 168 || $item_id == 169 || $item_id == 180 || $item_id == 181) ? "($result[size])" : "";
    
    header('Content-Type: text/html; charset=utf-8');
    date_default_timezone_set('Asia/Bangkok');
    $date = date("Y-m-d H:i:s");
    $sToken = $_ENV['LINE_TOKEN'];
    //$sToken = 'boQItS1RR1VCNJO6t2dPuJiuyg8sEZQeP4ukfAz9Uq0';
    $sMessage     = 'มีผู้แจ้งขอรับของรางวัลใหม่'."\n";
    $sMessage    .= 'วันที่ : '.$date."\n";
    $sMessage    .= 'ชื่อ-นามสกุล : '.$name."\n";
    $sMessage    .= 'อีเมล : '.$email."\n";
    $sMessage    .= 'สถานะของรางวัล : '.$level_title."\n";
    $sMessage    .= 'ของรางวัล : '.str_replace('%', 'Percent', $reward_items_title)." $size\n";
    $sMessage    .= 'สถานะ : Pending'."\n";
    $sMessage    .= 'Point : '.$reward_point."\n";

    $chOne = curl_init();
    curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
    curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt( $chOne, CURLOPT_POST, 1);
    curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=".$sMessage);
    $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$sToken.'', );
    curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec( $chOne );

    //Result error
    if(curl_error($chOne))
    {
      echo 'error:' . curl_error($chOne);
    }
    else {
      $result_ = json_decode($result, true);
    }
    curl_close( $chOne );
  }

}
