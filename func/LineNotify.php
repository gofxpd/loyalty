<?php
  $LineNotify = new LineNotify();
  $item_id = 168;
  $fncLineNotify = $LineNotify->fncLineNotify($item_id);
  $reward_items_title = $fncLineNotify['reward_items_title'];
  $level_title = $fncLineNotify['level_title'];
  $reward_level_title = $fncLineNotify['rewards_level_title'];
  $reward_point = number_format($fncLineNotify['rewards_level_point']);

  header('Content-Type: text/html; charset=utf-8');
  date_default_timezone_set('Asia/Bangkok');
  $date = date("Y-m-d H:i:s");
  $sToken = "uLJ6ZENrynnIW7JYi944qc5lfSF44CNvMFUk1jM7MOB";
  $sMessage     = 'มีผู้แจ้งขอรับของรางวัลใหม่'."\n";
  $sMessage    .= 'วันที่ : '.$date."\n";
  $sMessage    .= 'ชื่อ-นามสกุล : '.$name."\n";
  $sMessage    .= 'อีเมล : '.$email."\n";
  $sMessage    .= 'สถานะของรางวัล : '.$level_title."\n";
  $sMessage    .= 'ของรางวัล : '.$reward_items_title."\n";
  $sMessage    .= 'Point : '.$reward_point."\n";

  $chOne = curl_init();
  curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
  curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt( $chOne, CURLOPT_POST, 1);
  curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=".$sMessage);
  $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$sToken.'', );
  curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
  curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1);
  $result = curl_exec( $chOne );

  //Result error
  if(curl_error($chOne))
  {
    echo 'error:' . curl_error($chOne);
  }
  else {
    $result_ = json_decode($result, true);
  }
  curl_close( $chOne );
?>
