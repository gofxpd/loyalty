<?php
session_start();
include __DIR__ . '/function.php';
include __DIR__ . '/../components/HistoryCard/Card.php';
// Include Language file
if(isset($_SESSION['lang'])){
 include "../lang/".$_SESSION['lang']."/".$_SESSION['lang'].".php";
}else{
 include "../lang/th/th.php";
}

$date = intval($_GET['date']);
$email = $_GET['email'];
$lang = $_GET['lang'];
$history = new History();
$card = new Card();
$objHistory = $history->fncHistory($email,$date,$lang);

while($row = mysqli_fetch_array($objHistory)){
  $result = json_decode($row['value']);
  if ($row['type'] == "history") {
    echo $card->HistoryCard($row, $result, $lang);
  }
  else if ($row['type'] == "reset") {
    echo $card->ResetCard($row, $result);
  }
  else if ($row['type'] == "lot") {
    echo $card->CustomLotCard($row, $result);
  }
}
?>
