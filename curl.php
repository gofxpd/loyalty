<?php
  ob_start();
  session_start();
  include('func/function.php');

  require_once __DIR__ . '/func/auth/Login.php';
  require_once('./func/decrypt.php');

  $decrypt = new Decrypt();
  $local_date = date("Y-m-d");

  $data = str_replace('/curl.php?user=', '', $_SERVER['REQUEST_URI']);
  list($user_id, $user_date) = explode('&date=', $data, 2);
  $date = date('Y-m-d', $decrypt->decryptData($user_date));

  if($date == $local_date)
  {
    $user_id = $decrypt->decryptData($user_id);
    $auth = new Login($user_id);
    if (!$auth->load()) {
      header("Location:https://secure.gofx.com/sign-in.html", true, 301);
      exit();
    }
  }else{
    header("Location:https://secure.gofx.com/sign-in.html", true, 301);
    exit();
  }

  $lot = array();
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
  error_reporting(E_ERROR | E_PARSE);

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://servicefx.gofx.com/v1/royalty/get_user_data?user_id=" . $user_id,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_HTTPHEADER => array(
      "Content-Type: application/json"
    ),
  ));

  $response = curl_exec($curl);
  $json = json_decode($response);
  $error = curl_error($curl);

  //----- ตัวแปรจาก API -----//
  $avatar = $json->result->user_img;
  $email = $json->result->user_email;
  $fullname = $json->result->fullname;
  $birthday = $json->result->user_birthday;
  $gender = $json->result->user_sex;
  $status = $json->status_process;
  $_SESSION['avatar'] = $json->result->user_img;
  $_SESSION['email'] = $json->result->user_email;
  $_SESSION['name'] = $json->result->fullname;
  header("location:index");

?>
