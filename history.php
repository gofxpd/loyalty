<?php
  $page = 'history';
  include('template/header_temp.php');
  include __DIR__ . '/components/HistoryCard/Card.php';
  $user_email = $_SESSION['email'];
?>
<? (isset($_SESSION['lang']) ? $lang = $_SESSION['lang'] : $lang = 'th'); ?>
<style>
  .text-pending {
    color: #13A0D5;
  }
  .text-progress {
    color: #E2AE01;
  }
  .text-success {
    color: #1CA303;
  }
  .text-danger {
    color: #E2063C;
  }
  .w-card {
    height: 50px;
    display: flex;
    align-items: center;
  }
</style>

<div class="container mgtb-3">

  <div class="row history-content">
    <div class="col-lg-7 col-md-6 col-sm-5 col-12">
      <h3 class="history-title"><?= _HISTORY_HEAD ?></h3>
    </div>
    <div class="col-lg-5 col-md-6 col-sm-7 col-12 select-pd">
      <div class="dropdown history_dropdown">
        <i class="fas fa-calendar-alt fa-2x" aria-hidden="true"></i>
        <button class="btn dropdown-toggle history_dropdown_btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Last 120 days
        </button>
        <div class="dropdown-menu dropdown-history-menu history_datepicker" aria-labelledby="dropdownMenuButton">
          <ul>
            <li class="dropdown-item history_range" value="30" onclick="history_range(this.value)">Last 30 days</li>
            <li class="dropdown-item history_range" value="60" onclick="history_range(this.value)">Last 60 days</li>
            <li class="dropdown-item history_range" value="90" onclick="history_range(this.value)">Last 90 days</li>
            <li class="dropdown-item history_range" value="120" onclick="history_range(this.value)" >Last 120 days</li>
          </ul>
          <hr>
          <div class="row">
            <div class="col-12">
              <form method="post">
                <div class="form-group">
                  <label for="exampleFormControlInput1"><?= _HISTORY_SELECT_TEXT_1 ?></label>
                  <input type="date" name="start_date" id="start_date" data-date-format="d-M-Y" value="<?= date("Y-m-d"); ?>" class="form-control">
                </div>
                <div class="form-group">
                  <label for="exampleFormControlSelect1"><?= _HISTORY_SELECT_TEXT_2 ?></label>
                  <input type="date" name="end_date" id="end_date" min="" data-date-format="d-M-Y" value="<?= date("Y-m-d"); ?>" class="form-control">
                </div>
                <button type="submit" name="submit" class="btn btn-history btn-block"><?= _HISTORY_SELECT_BUTTON_1 ?></button>
                <button type="close" class="btn btn-light btn-block"><?= _HISTORY_SELECT_BUTTON_2 ?></button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php
    $date = 120;
    $card = new Card();
    $CntHistory = new History();
    $objCntHistory = $CntHistory->fncCntHistory($email,$date,$lang);
    $cnt = $objCntHistory['cnt'];
    ($cnt == 0 ? $empty = 'history-empty': $empty = '');
  ?>
  <div class="container mgt-3 history <?= $empty ?>" id="History-receive">
    <?php
    if(!isset($_POST['submit'])){
      $history = new History();
      $objHistory = $history->fncHistory($email,$date,$_SESSION['email']);
      while($row = mysqli_fetch_array($objHistory)){
        $result = json_decode($row['value']);
        if ($row['type'] == "history") {
          echo $card->HistoryCard($row, $result, $email);
        }
        else if ($row['type'] == "reset") {
          echo $card->ResetCard($row, $result);
        }
        else if ($row['type'] == "lot") {
          echo $card->CustomLotCard($row, $result);
        }
      }
    }else{
      $start_date = $_POST['start_date'];
      $end_date = $_POST['end_date'];
      $s_date = date("d.m.y", strtotime($start_date));
      $e_date = date("d.m.y", strtotime($end_date));
      echo "<script>
          $(document).ready(function(){
            localStorage.setItem('Start_date', '$s_date');
            localStorage.setItem('End_date', '$e_date');

            var start_date = localStorage.getItem('Start_date');
            var end_date = localStorage.getItem('End_date');
            document.querySelector('.history_dropdown_btn').innerText = start_date+' - '+end_date;
          });
          </script>";

      $history = new History();
      $objHistory = $history->fncBetweenHistory($email,$start_date,$end_date,$lang);
      while($row = mysqli_fetch_array($objHistory)){
        $result = json_decode($row['value']);
        if ($row['type'] == "history") {
          echo $card->HistoryCard($row, $result, $lang);
        }
        else if ($row['type'] == "reset") {
          echo $card->ResetCard($row, $result);
        }
        else if ($row['type'] == "lot") {
          echo $card->CustomLotCard($row, $result);
        }
      }
    }

    function actionBy($val) {
      return !empty($val) ? _HISTORY_BOX_TEXT_4 : _HISTORY_BOX_TEXT_3;
    }

    ?>

  </div>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<?php include('template/footer_temp.php') ?>

<script type="text/javascript">
  $(document).ready(function(){
    localStorage.removeItem('id_active');
  });

  $('#start_date').change(function() {
     var end_date = document.getElementById("end_date");
     end_date.setAttribute("min", this.value);
  });

function history_range(date){
  var email = "<?= $email ?>";
  var lang = "<?= $lang ?>";
  if (date == "") {
    document.getElementById("History-receive").innerHTML = "";
    return;
  } else {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("History-receive").innerHTML = this.responseText;
      }
    };
    document.querySelector('.history_dropdown_btn').innerText = 'Last '+date+' days ';
    xmlhttp.open("GET","func/GetData.php?date="+date+"&email="+email+"&lang="+lang, true);
    xmlhttp.send();
  }
}
 
</script>
