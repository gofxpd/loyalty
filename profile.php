<?php
  $page = 'profile';
  include_once 'template/header_temp.php';
  (isset($_SESSION['lang']) ? $lang = $_SESSION['lang'] : $lang = 'th');
?>
<div class="container mgtb-3">
  <h3 class="profile-head">จัดการข้อมูล</span></h3>
  <div class="row  mgt-2 mobile-version-detail">
    <div class="col-lg-12 col-md-12 col-12 pd-reward-detail"></div>
    <div class="col-lg-12 col-md-12 col-12 bg-reward-detail-items-1 profile-desc-box">
      <p><?= _PROFILE_DESCRIPTION ?></p>
    </div>
    <div class="item reward-detail col-lg-12">
        <div class="panel panel-default">
          <div class="row bg-reward-detail-items-2 profile-detail-box">
            <div class="col-lg-6 col-12 profile-detail-image">
              <img src="images/profile/user_profile.png" alt="" class="profile-img">
            </div>
            <div class="col-lg-6 col-12">
              <?php
                $GetAddress = new UserAddress();
                $objGetAddress = $GetAddress->fncGetAddress($email,$lang);
                $tel = $objGetAddress['tel'];
                $address =  $objGetAddress['user_add'];
               ?>
              <div class="row mgb-05">
                <div class="col-12 profile-detail">
                  <h4><?= _PROFILE_DETAIL_HEAD ?></h4>
                </div>
              </div>

              <div class="row mgb-05">
                <div class="col-lg-5 col-md-4 col-12 profile-title">
                  <h4><i class="fas fa-user-circle 2xl"></i> <?= _PROFILE_DETAIL_TEXT_1 ?><h4>
                </div>
                <div class="col-lg-7 col-md-8 col-12 profile-detail">
                  <p><?= $name ?></p>
                </div>
              </div>

              <div class="row mgb-05">
                <div class="col-lg-5 col-md-4 col-12 profile-title">
                  <h4><i class="fas fa-envelope"></i> <?= _PROFILE_DETAIL_TEXT_3 ?><h4>
                </div>
                <div class="col-lg-7 col-md-8 col-12 profile-detail">
                  <p><?= $email ?></p>
                </div>
              </div>

              <div class="row mgb-05">
                <div class="col-lg-5 col-md-4 col-12 profile-title">
                  <h4><i class="fas fa-flag"></i> <?= _PROFILE_DETAIL_TEXT_4 ?><h4>
                </div>
                <div class="col-lg-7 col-md-8 col-12 profile-detail">
                  <p>ไทย</p>
                </div>
              </div>
              <div
              id="data-region"
              data-province="<?= $objGetAddress['province_id'] ?>"
              data-amphure="<?= $objGetAddress['amphure_id'] ?>"
              data-district="<?= $objGetAddress['district_id'] ?>"
              ></div>
              <div class="row mgb-05">
                <div class="col-lg-5 col-md-4 col-12 profile-title">
                  <h4><i class="fas fa-phone-alt"></i> <?= _PROFILE_DETAIL_TEXT_2 ?><h4>
                </div>
                <div class="col-lg-7 col-md-8 col-12 profile-detail">
                  <p><?= (!isset($tel) || $tel == '' ? "<span>กรุณาระบุข้อมูล</span>" : $tel ) ?> </p>
                </div>
              </div>

              <div class="row mgb-1">
                <div class="col-lg-5 col-md-4 col-12 profile-title">
                  <h4><i class="fas fa-map-marker-alt"></i> <?= _PROFILE_DETAIL_TEXT_5 ?><h4>
                </div>
                <div class="col-lg-7 col-md-8 col-12 profile-detail">
                  <p><?= (!isset($address) || $address == '' ? "<span>กรุณาระบุข้อมูล</span>" : $address ) ?></p>
                </div>
              </div>
              <!-- Start modal edit form -->
              <div class="row">
                <div class="col-lg-5 col-md-6 col-12 mb-ver-none">
                  <button type="button" class="btn btn-light btn-edit-address" onclick="window.location.href='rewards'"><?= _PROFILE_DETAIL_TEXT_12 ?></button>
                </div>
                <div class="col-lg-7 col-md-6 col-12">
                  <button type="button" class="btn btn-danger btn-edit-address" id="show-form"><span class="mb-ver-none"><?= _PROFILE_DETAIL_TEXT_11 ?></span> <span class="mb-ver"><?= _PROFILE_DETAIL_TEXT_13 ?></span></button>
                </div>
                <div class="col-lg-5 col-md-6 col-12 mb-ver mgt-1">
                  <button type="button" class="btn btn-light btn-edit-address" onclick="window.location.href='rewards'"><?= _PROFILE_DETAIL_TEXT_12 ?></button>
                </div>
              </div>
              <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="<?= $_SESSION['email']; ?>" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-md modal-address-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body">
                      <div class="container pdall-2 mgb-1">
                        <h3 class="form-address-head"><?= _FORM_ADDRESS_HEAD ?></h3>
                        <form method="post" id="formAddress">
                          <div class="row">
                            <div class="form-group col-lg-6">
                              <label for="exampleInputEmail1"><?= _PROFILE_DETAIL_TEXT_1 ?></label>
                              <input type="text" class="form-control" id="name" value="<?= $name ?>" disabled>
                            </div>
                            <div class="form-group col-lg-6">
                              <label for="exampleInputEmail1"><?= _PROFILE_DETAIL_TEXT_3 ?></label>
                              <input type="text" class="form-control" id="email" value="<?= $email ?>" disabled>
                            </div>
                            <div class="col-lg-6 mgb-1">
                              <label for=""><?= _PROFILE_DETAIL_TEXT_10 ?></label>
                              <input type="text" class="form-control" name="address" id="address" required>
                            </div>
                            <div class="col-md-6 mgb-1">
                              <label for="Province"><?= _PROFILE_DETAIL_TEXT_6 ?></label>
                              <select class="form-control" name="province" id="province">
                                <option>เลือกจังหวัด</option>
                              </select>
                            </div>
                            <div class="col-md-6 mgb-1">
                              <label for="Amphure"><?= _PROFILE_DETAIL_TEXT_7 ?></label>
                              <select class="form-control" name="amphure" id="amphure">
                                <option>เลือก เขต/อำเภอ</option>
                              </select>
                            </div>
                            <div class="col-md-6 mgb-1">
                              <label for="District"><?= _PROFILE_DETAIL_TEXT_8 ?></label>
                              <select class="form-control" name="district" id="district">
                                <option>เลือก แขวง/ตำบล</option>
                              </select>
                            </div>
                            <div class="col-md-6 mgb-1">
                              <label for=""><?= _PROFILE_DETAIL_TEXT_9 ?></label>
                              <input type="text" class="form-control" name="zipcode" id="zipcode" readonly>
                            </div>
                            <div class="col-md-6 mgb-1">
                              <label for=""><?= _PROFILE_DETAIL_TEXT_2 ?></label>
                              <input type="tel" class="form-control" name="tel" id="tel" value="<?= $tel ?>" required>
                            </div>
                            <input type="hidden" name="email" value="<?= $_SESSION['email']; ?>">
                            <div class="col-md-12 text-right mgt-05">
                              <button type="button" class="btn btn-light btn-profile-close mb-ver-none" data-dismiss="modal"><?= _REWARD_MODAL_TEXT_5 ?></button>
                              <input type="submit" value="บันทึกข้อมูล" class="btn btn-danger btn-profile-submit">
                              <button type="button" class="btn btn-light btn-profile-close mb-ver mgt-1" data-dismiss="modal"><?= _REWARD_MODAL_TEXT_5 ?></button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End modal edit form -->
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModalSuccess" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalSuccessTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-md modal-confirm-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      </div>
      <div class="modal-body">
        <div class="container pdall-2">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
              <button type="button btn-success-close" onclick="HideSuccessModal()" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="modal-cancel"><i class="fa fa-times" aria-hidden="true"></i></span>
              </button>
              <p class="success-text-1 mb-ver-none"><?= _UPDATE_PROFILE_TEXT_1 ?></p>
              <p class="success-text-1 mb-ver"><?= _UPDATE_PROFILE_TEXT_7 ?></p>
              <p class="success-text-2"><?= _UPDATE_PROFILE_TEXT_2 ?></p>
              <div class="text-center mgb-3">
                <img src="images/messageImage_1622529208597.jpg" class="img-fulid reward-detail-success-img">
              </div>
              <p class="success-text-4"><?= _UPDATE_PROFILE_TEXT_3 ?></p>
              <div class="row">
                <div class="col-12 mgb-1 mb-ver">
                  <button type="button" class="btn btn-danger btn-lg success-text-5 btn-modal-success text-center" data-dismiss="modal"><?= _REWARD_SUCCESS_TEXT_5 ?></button>
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                  <button type="button" class="btn btn-light btn-lg success-text-5 btn-modal-success text-center" onclick="window.location.href='rewards'"><?= _UPDATE_PROFILE_TEXT_4 ?></button>
                </div>
                <div class="col-lg-6 col-md-6 col-12 mb-ver-none">
                  <button type="button" class="btn btn-danger btn-lg success-text-5 btn-modal-success text-center" data-dismiss="modal"><?= _REWARD_SUCCESS_TEXT_5 ?></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script>
  // Check validation
  $("#tel").keypress(function(event) {
    if (event.keyCode >= 48 && event.keyCode<=57) {
        return
    }
    else {
      event.preventDefault();
    }
  });
  $('#tel').attr('maxlength', '10')

  // Modal form link reward detail
  $(document).ready(function(){
    var modal_status = localStorage.getItem('modal_status');
    if(modal_status){
      $('#modal-form').modal('show');
      localStorage.removeItem('modal_status');
    }
  });

  $('#exampleModalSuccess').on('hidden.bs.modal', function () {
    location.reload();
  })

  $(document).ready(function() {
    provinces();

    $('#province').change(function () {
      amphures($(this).val());
    });

    $('#amphure').change(function() {
      districts($('#amphure').val());
      zipcode($('#district').val());
    });

    $('#district').change(function() {
      zipcode($('#district').val());
    });
  });


  $('#show-form').click(function() {
    $.ajax({
      url: '/func/history/index.php',
      method: 'get',
      data: {email:$('#modal-form').attr('aria-labelledby')},
      dataType: 'json',
      success: function(data) {
        $('#modal-form').modal('show');
        $('#address').val(data.address);
        provinces(data.province);

        setTimeout(() => {
          amphures($('#province').val());
        }, 100);
      }
    });
  });

  $('#formAddress').submit(function(e) {
    e.preventDefault();

    $.ajax({
      url: '/func/history/store.php',
      method: 'post',
      data: new FormData(this),
      cache: false,
      contentType: false,
      processData: false,
      success: function(data) {
        $('#exampleModalSuccess').modal('show');
        $('#modal-form').modal('hide');
      }
    });
  });

  function HideSuccessModal(){
    location.reload();
  }

  const provinces = (id = null) => {
    $.ajax({
      url: '/func/region/province.php',
      method: 'get',
      dataType: 'json',
      success: function(data) {
        data.map(province => $('#province').append(`<option value='${province.id}' ${id == province.id ? 'selected' : ''}>${province.name_th}</option>`));
      }
    });
  }

  const amphures = (id) => {
    const amphure_id = $('#data-region').attr('data-amphure');

    $.ajax({
      url: '/func/region/amp.php',
      method: 'get',
      data: {id:id},
      dataType: 'json',
      success: function(data) {
        optionHtml($('#amphure'));
        data.map(amphure => $('#amphure').append(`<option value='${amphure.id}' ${amphure_id == amphure.id ? 'selected' : ''}>${amphure.name_th}</option>`));
        districts($('#amphure').val());
      }
    });
  }

  const districts = (id) => {
    const district_id = $('#data-region').attr('data-district');

    $.ajax({
      url: '/func/region/dist.php',
      method: 'get',
      data: {id:id},
      dataType: 'json',
      success: function(data) {
        optionHtml($('#district'));
        data.map(district => $('#district').append(`<option value='${district.id}' ${district_id == district.id ? 'selected' : ''}>${district.name_th}</option>`));
        zipcode($('#district').val());
      }
    });
  }

  const zipcode = (id) => {
    $.ajax({
      url: '/func/region/zipc.php',
      method: 'get',
      data: {id:id},
      dataType: 'json',
      success: function(data) {
        $('#zipcode').val(data.zip_code);
      }
    });
  }

  const optionHtml = (data) => {
    data.html("");
  }
</script>

<?php include('template/footer_temp.php') ?>
