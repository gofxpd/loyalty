<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>GOFX LOYALTY PROGRAM</title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="shortcut icon" href="images/ily.png" />
  <style>
    .btn-danger{
      background-color: #ce202e;
      width: 70%;
      font-size: 18px;
      font-weight: 600;
    }
    .btn-danger:hover{
      background-color: #ba0312;
    }
    .logo{
      margin: auto;
      display: block;
      margin-bottom: 4em;
    }
    .form-control{
      width: 70%;
      display: unset;
    }

    #login {
      height: 100vh;
      display: flex;
      align-items: center;
    }
  </style>
</head>
<body>
  <!-- <form action="func/login.php" method="POST">
    <input type="text" name="email">
    <input type="submit" value="Login">
  </form> -->
  <div id="login">
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form action="func/login.php" method="POST" id="login-form" class="form">
                            <img src="images/logo.png" class="img-fulid logo">
                            <div class="form-group text-center">
                                <input type="text" name="email" id="email" class="form-control" placeholder="Email">
                            </div>
                            <div id="register-link" class="text-center">
                              <button type="submit" class="btn btn-danger">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>