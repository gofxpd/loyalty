<?php
  // nav bar //
  define("_SIGNIN_LINK", "https://secure.gofx.com/th/sign-in.html");
  // define("_SIGNIN_LINK", "login");
  define("_REGISTER_LINK", "https://secure.gofx.com/th/newAccount.html");
  define("_SIGNIN", "เข้าสู่ระบบ");
  define("_REGISTER", "สมัครสมาชิก");
  define("_SIGNOUT", "ออกจากระบบ");
  define("_PROFILE", "จัดการข้อมูล");

  // Checkin //
  define("_CHECKIN_CONFIRM_TEXT1", "ยินดีด้วยคุณได้รับของรางวัล");
  define("_CHECKIN_CONFIRM_TEXT2", "รับรางวัลของคุณ");
  define("_CHECKIN_CONFIRM_TEXT3", "การรับของรางวัล GOFY CHECK-IN BONUS ทาง GOFX จะดำเนินการจัดส่งของ รางวัลภายใน 15 วัน 
                                    หลังวันหมดเขตการลงทะเบียนร่วมกิจกรรม GOFX ขอขอบคุณลูกค้า  ทุกท่านที่ร่วมสนุกกับกิจกรรมครั้งนี้");
  define("_CHECKIN_CONFIRM_TEXT4", "เสื้อยืดพรีเมียม Crypto Shirt โชว์ความเป็นเทคโนโลยี Blockchain Generation ใหม่ ยกมาแทบทุกเหรียญมีด้วยกัน 4 ลาย 
                                    ได้แก่ 1. Doge Shirt 2. Bit Shirt 3. XRP Shirt 4. ETH Shirt เนื้อผ้า Cotton 100 % Unisex ใส่ได้ทั้งชายและหญิง 
                                    (บริษัทฯ ขอสงวนสิทธิ์ทางลูกค้าไม่สามารถเลือกสีและลายของรางวัลมา ณ ทีนี้ได้)");                                  
  define("_CHECKIN_CONFIRM_TEXT5", "ปิด");
  define("_CHECKIN_CONFIRM_TEXT6", "ยืนยันไซต์เสื้อ");
  define("_CHECKIN_GUEST_TEXT1", "GOFY CHECK-IN BONUS");
  define("_CHECKIN_GUEST_TEXT2", "วิธีแลกของรางวัล");
  define("_CHECKIN_GUEST_TEXT3", "1. กดรับของรางวัลรายวัน");
  define("_CHECKIN_GUEST_TEXT4", "สามารถกดรับของรางวัล หรือ Check-in ตามวันที่กำหนด");
  define("_CHECKIN_GUEST_TEXT5", "2. ยินดีด้วยท่านได้รับของรางวัล");
  define("_CHECKIN_GUEST_TEXT6", "ท่านสามารถกดรับรางวัลได้วันละ 1 ครั้ง และสูงสุด 7 วัน");
  define("_CHECKIN_GUEST_TEXT7", "3. รับของรางวัลเสร็จสิ้น");
  define("_CHECKIN_GUEST_TEXT8", "ท่านจะได้รับของรางวัลภายใน 15 วันหลังจากหมดเขตร่วมกิจกรรม");
  define("_CHECKIN_GUEST_TEXT9", "วิธีการร่วมกิจกรรม");
  define("_CHECKIN_GUEST_TEXT10", "<span>1</span> เปิดบัญชีจริงกับ <b>GOFX</b> และมีการฝากเงินขั้นต่ำ <b>100 USD</b>");
  define("_CHECKIN_GUEST_TEXT11", '<span>2</span> ต้องมีการเปิดออเดอร์อย่างน้อยหนึ่งออเดอร์ไม่จำกัดจำนวน Lot');
  define("_CHECKIN_GUEST_TEXT12", "<span>3</span> ทำการ <b>Login</b> เข้าสู่ระบบ <b>Loyalty Program</b> และสามารถร่วมกิจกรรม <b>GOFY CHECK-IN BONUS</b> ได้");
  define("_CHECKIN_GUEST_TEXT13", "<span>4</span> กิจกรรม <b>GOFY CHECK-IN BONUS</b> สามารถกดรับรางวัลได้สูงสุดถึง <b>7 รางวัล ภายใน 30 วัน</b>");
  define("_CHECKIN_GUEST_TEXT14", "<span>5</span> ระยะเวลาลงทะเบียนร่วมกิจกรรม <b>ตั้งแต่วันนี้ - 30 พฤศจิกายน 2564</b>");
  define("_CHECKIN_GUEST_TEXT15", "เปิดบัญชี");
  define("_CHECKIN_GUEST_TEXT16", "วิธีการฝากเงิน");
  define("_CHECKIN_CONDITIONS_TEXT1", "เงื่อนไขการร่วมกิจกรรม <br class='mb-ver'> GOFY CHECK-IN BONUS");
  define("_CHECKIN_CONDITIONS_TEXT2", "อ่านรายละเอียดเงื่อนไขและวิธีการร่วมกิจกรรมทั้งหมด");
  define("_CHECKIN_CONDITIONS_TEXT3", "1. ต้องมีบัญชีจริงกับ GOFX และมีการฝากเงินขั้นต่ำ 100 USD");
  define("_CHECKIN_CONDITIONS_TEXT4", "2. ต้องมีการเปิดออเดอร์อย่างน้อย 1 ออเดอร์ ไม่จำกัดจำนวน Lot");
  define("_CHECKIN_CONDITIONS_TEXT5", "3. ทำการ Login เข้าสู่ระบบ Loyalty Program และสามารถร่วมกิจกรรม GOFY CHECK-IN BONUS ได้");
  define("_CHECKIN_CONDITIONS_TEXT6", "4. กิจกรรม GOFY CHECK-IN BONUS สามารถกดรับรางวัลได้สูงสุดถึง 7 รางวัล ภายใน 30 วัน");
  define("_CHECKIN_CONDITIONS_TEXT7", "5. บริษัทฯ ขอสงวนสิทธิ์ในการเลือกสีและลายของรางวัล ซึ่งลูกค้าไม่สามารถเลือกได้ ของรางวัลจึงเป็นไปตามที่บริษัทฯ กำหนด");
  define("_CHECKIN_CONDITIONS_TEXT8", "6. ทางบริษัทฯ จะกำหนดส่งของรางวัลภายใน 15 วัน หลังวันหมดเขตการลงทะเบียนร่วมกิจกรรม");
  define("_CHECKIN_CONDITIONS_TEXT9", "7. ระยะเวลาลงทะเบียนร่วมกิจกรรม ตั้งแต่วันนี้ - 30 พฤศจิกายน 2564");
  define("_CHECKIN_MB_BUTTON_TEXT1", "รายละเอียดรางวัล");
  define("_CHECKIN_MB_BUTTON_TEXT2", "เงื่อนไขกิจกรรม");
  define("_CHECKIN_MB_CONDITION_TEXT1", "อ่านรายละเอียด");
  
  // Main Menu //
  define("_MAINMENU_INDEX", "หน้าแรก");
  define("_MAINMENU_REWARDS", "ของรางวัลทั้งหมด");
  define("_MAINMENU_GOFY", "CHECK-IN");
  define("_MAINMENU_HISTORY", "ประวัติการแลก");
  define("_MAINMENU_TERM", "ข้อตกลงและเงื่อนไข");
  define("_MAINMENU_CONTACT", "ติดต่อเรา");
  define("_LOGOUT", "ออกจากระบบ");
  define("_Flag", "Thailand.png");

  // Mobile Menu //
  define("_MOBILEMENU_INDEX", "หน้าแรก");
  define("_MOBILEMENU_REWARDS", "รางวัลทั้งหมด");
  define("_MOBILEMENU_ACCOUNT", "เปิดบัญชี");
  define("_MOBILEMENU_TERMS", "ข้อตกลง");
  define("_MOBILEMENU_NAV", "เมนู");

  // Banner desktop, tablet //
  define("_BANNER_HEADTEXT1", "LOYALTY PROGRAM");
  define("_BANNER_HEADTEXT2", "สถานะลอยัลตี้ของคุณ");
  define("_BANNER_HEADTEXT3", "จำนวนคะแนนสะสม");
  define("_BANNER_TEXT1", " โปรโมชั่นสุดพิเศษเพื่อตอบแทนเทรดเดอร์ ผู้ใช้บริการของ GOFX.com ยกระดับการลงทุนของคุณให้คุ้มค่ายิ่งขึ้น รับรางวัลมากมายแบบไม่ต้องรอลุ้น
  ไม่ว่าจะเป็น Gadget สุดทันสมัย ไปจนถึงรถยนต์สุดหรูหลากหลายแบรนด์เช่น Maserati, Porshe, คูปองเงินสด และของรางวัลอีกมากมายที่เราคัดสรรมาเพื่อคุณโดยเฉพาะ");
  define("_BANNER_TEXT2", "Master Trader");
  define("_BANNER_TEXT3", "Point สะสมลอยัลตี้ของคุณ :");
  define("_BANNER_TEXT4", "Lot สะสมลอยัลตี้ของคุณ :");

  // Banner mobile //
  define("_BANNER_MB_HEADTEXT3", "คะแนนสะสม");
  define("_BANNER_MB_TEXT1", "โปรโมชั่นสุดพิเศษเพื่อตอบแทนเทรดเดอร์ ผู้ใช้บริการของ GOFX.com ยกระดับการลงทุนของคุณให้คุ้มค่ายิ่งขึ้น รับรางวัลมากมายแบบไม่ต้องรอลุ้น ที่เราคัดสรรมาเพื่อคุณโดยเฉพาะ");
  define("_BANNER_MB_TEXT3", "GO Point : ");
  define("_BANNER_MB_TEXT4", "Lot สะสม : ");

  // Footer //
  define("_FOOTER_MAIN1", "บัญชีซื้อขาย");
  define("_FOOTER_MAIN2", "การซื้อขายตราสาร");
  define("_FOOTER_MAIN3", "เงื่อนไขการซื้อขาย");
  define("_FOOTER_MAIN4", "MT4");
  define("_FOOTER_MAIN5", "Contact Us");

  define("_FOOTER_MAIN1_1", "ประเภทของบัญชีซื้อขาย");
  define("_FOOTER_MAIN2_1", "Forex");
  define("_FOOTER_MAIN2_2", "CFD ดัชนี");
  define("_FOOTER_MAIN2_3", "CFD พลังงาน");
  define("_FOOTER_MAIN2_4", "CFD โลหะมีค่า");
  define("_FOOTER_MAIN2_5", "CFD หุ้น");
  define("_FOOTER_MAIN2_6", "CFD Cryptocurrency");
  define("_FOOTER_MAIN3_1", "นโยบายความเป็นส่วนตัว");
  define("_FOOTER_MAIN3_2", "ข้อตกลงและเงื่อนไข");
  define("_FOOTER_MAIN3_3", "เวลาซื้อขาย");
  define("_FOOTER_MAIN3_4", "คำถามที่พบบ่อย");
  define("_FOOTER_MAIN4_1", "MT4 สำหรับ PC");
  define("_FOOTER_MAIN4_2", "MT4 สำหรับ iPhone");
  define("_FOOTER_MAIN4_3", "MT4 สำหรับ Android");
  define("_FOOTER_MAIN5_1", "ติดต่อเรา");

  // หนเ้าแรก //
  define("_REWARD_TEXT_1", "คะแนนสะสม");
  define("_REWARD_TEXT_2", "สถานะลอยัลตี้");
  define("_REWARD_TEXT_3", "Point");
  define("_REWARD_TEXT_4", "Lot : GO Point");
  define("_REWARD_LOT_GO", "1 Lot = 10 Point");
  define("_REWARD_POINT_GO", "0 - 1.8K Point");
  define("_REWARD_LOT_ADVANCE", "1 Lot = 12 Point");
  define("_REWARD_POINT_ADVANCE", "2.8K - 105K Point");
  define("_REWARD_LOT_EXPERT", "1 Lot = 15 Point");
  define("_REWARD_POINT_EXPERT", "116K - 540K Point");
  define("_REWARD_LOT_MASTER", "1 Lot = 20 Point");
  define("_REWARD_POINT_MASTER", "585K - 30M Point");
  define("_REWARDTIMELINE_TEXT", "รวมรางวัลสถานะ");
  define("_REWARDTIMELINE_TEXT_GO", "รวมรางวัลสถานะ <br class='tab-ver tab-lan mb-ver mb-lan'><span class='mb-index-timeline-text'>GO Trader</span>");
  define("_REWARDTIMELINE_TEXT_ADVANCE", "รวมรางวัลสถานะ <br class='tab-ver tab-lan mb-ver mb-lan'><span class='mb-index-timeline-text'>Advance Trader</span>");
  define("_REWARDTIMELINE_TEXT_EXPERT", "รวมรางวัลสถานะ <br class='tab-ver tab-lan mb-ver mb-lan'><span class='mb-index-timeline-text'>Expert Trader</span>");
  define("_REWARDTIMELINE_TEXT_MASTER", "รวมรางวัลสถานะ <br class='tab-ver tab-lan mb-ver mb-lan'><span class='mb-index-timeline-text'>Master Trader</span>");
  define("_REWARDTIMELINE_Go", "ก้าวแรกของการเทรด เริ่มต้นเส้นทางของคุณด้วยของรางวัลสุดพรีเมียมจาก GOFX และของรางวัลอื่น ๆ อีกมากมาย ตอบแทนความทุ่มเทของคุณ ทลายกำแพง 
                                พร้อมปลดล็อคไปอีกขั้น พิสูจน์ตัวตนคุณไปพร้อมกับ GOFX เราพร้อมจะอยู่เคียงข้างคุณตลอดเส้นทางการเทรด");
  define("_REWARDTIMELINE_Advance", "รางวัลฉลองก้าวที่ 2 ของคุณ ยกระดับความท้าทายของคุณ ทาง GOFX ได้คัดสรรของรางวัลสุดพิเศษ เอาใจเทรดเดอร์ทุกสาย ไม่ว่าจะเป็น สาย beauty,
  สายสุขภาพ, สาย Gadgets หรือสายท่องเที่ยว และคุณยังสามารถแลกคะแนนเป็นคูปองเงินสดได้แล้ว! ถึงเวลาปลดล็อค เพลิดเพลินกับของรางวัลในทุกขั้น กับ GOFX");
  define("_REWARDTIMELINE_Expert", "ก้าวขึ้นมาอีกขั้น เตรียมพร้อมขึ้นสู่ จุดสูงสุด ในระดับนี้คุณจะสามารถยกระดับความสมบูรณ์แบบขึ้นไปอีก GOFX
  ใส่ใจคุณทุกย่างก้าวและขอมอบรางวัลตอบแทนให้คุณอย่างคุ้มค่า Expert Trader จะได้รับรางวัลสุดหรูหรามากมาย สำหรับทุกเพศทุกวัย ไม่ว่าจะเป็น สร้อยคอ Channel, IMAC รุ่นใหม่ล่าสุด,
  Ninebot Gokart PRO หรือ TV UHD QLED สุดพรีเมียม");
  define("_REWARDTIMELINE_Master", "ยินดีด้วย! คุณได้ปลดล็อคมาจนถึงระดับสูงสุด Master trader คือสถานะสุด Exclusive ที่คุณจะได้รับสิทธิพิเศษต่าง ๆ มากมาย
  และคุณก็ยังคงสามารถปลดล็อครางวัลสุด exclusive ได้อย่างต่อเนื่อง ไม่ว่าจะเป็น ทริป Private jet สุดหรู, Porshe911 หรือ Maserati เติมเต็มความสมบูรณ์แบบให้ถึงขีดสุด");
  define("_REWARD_BUTTON", "ดูรายละเอียดของรางวัล");
  define("_REWARDTIMELINE_BUTTON", "แลกรับของรางวัล");
  define("_LOYALTY_VIDEO", "ยิ่งเทรด ยิ่งได้มาก");
  define("_LOYALTY_VIDEO_TEXT", "GOFX ให้คุณสัมผัสกับประสบการณ์ใหม่ <br class='mb-ver'>รับความพิเศษเหนือระดับกับ LOYALTY PROGRAM");
  define("_LOYALTY_SLIDER", "ภาพกิจกรรมรับรางวัล");
  define("_LOYALTY_PROMOTION", "สิทธิประโยชน์ & โปรโมชั่น อีกมากมาย");
  define("_LOYALTY_ABOUTUS", "เกี่ยวกับเรา");
  define("_LOYALTY_ABOUTUS_TEXT", 'GOFX Limited คือ "เทรดดิ้งแพลตฟอร์ม" ที่ก่อตั้งขึ้นโดยทีมงานผู้บริหารที่มีความเชี่ยวชาญในด้านเทคโนโลยีทางการเงินและกลุ่มเทรดเดอร์ผู้มีประสบการณ์
                                  ระดับมืออาชีพในตลาดซื้อขายสากล เราจึงเข้าใจนักลงทุนมากที่สุด และGOFX Limited ยังเชื่อว่าการเงินเป็นเรื่องของทุกคน เราจึงมุ่งมั่นออกแบบเทรดดิ้ง
                                  แพลตฟอร์มและกิจกรรมสนับสนุน ที่เหมาะสมกับความต้องการของเทรดเดอร์ในแต่ละพื้นที่การใช้งานอย่างเข้าใจ เราพร้อมจะอยู่เคียงข้างคุณตลอดเส้นทางการลงทุน');

  // หนเ้าแรก mobile //
  define("_REWARDTIMELINE_MB_TEXT", "รางวัลสถานะ");
  define("_REWARDTIMELINE_MB_Go", "ก้าวแรกของการเทรด เริ่มต้นเส้นทางของคุณด้วยของรางวัลสุดพรีเมี่ยมจาก GOFX และของรางวัลอื่นๆอีกมากมาย");
  define("_REWARDTIMELINE_MB_Advance", "รางวัลฉลองก้าวที่ 2 ของคุณ ยกระดับความท้าทายของคุณ ทาง GOFX ได้คัดสรรของรางวัลสุดพิเศษ เอาใจเทรดเดอร์ทุกสาย");
  define("_REWARDTIMELINE_MB_Expert", "คุณจะสามารถยกระดับความสมบูรณ์แบบขึ้นไปอีก GOFX ใส่ใจคุณทุกย่างก้าวและขอมอบรางวัลตอบแทนให้คุณอย่างคุ้มค่า");
  define("_REWARDTIMELINE_MB_Master", "ยินดีด้วย! คุณได้ปลดล็อคมาจนถึงระดับสูงสุด Master trader คือสถานะสุด Exclusive ที่คุณจะได้รับสิทธิพิเศษต่าง ๆ มากมาย");

  // ข้อมูลส่วนตัว //
  define("_PROFILE_DETAIL_HEAD", "ข้อมูลผู้ถือบัญชี");
  define("_PROFILE_DETAIL_TEXT_1", "ผู้ถือบัญชี");
  define("_PROFILE_DETAIL_TEXT_2", "เบอร์โทรศัพท์");
  define("_PROFILE_DETAIL_TEXT_3", "อีเมล");
  define("_PROFILE_DETAIL_TEXT_4", "ประเทศ");
  define("_PROFILE_DETAIL_TEXT_5", "ที่อยู่รับของรางวัล");
  define("_PROFILE_DETAIL_TEXT_6", "จังหวัด");
  define("_PROFILE_DETAIL_TEXT_7", "เขต/อำเภอ");
  define("_PROFILE_DETAIL_TEXT_8", "แขวง/ตำบล");
  define("_PROFILE_DETAIL_TEXT_9", "รหัสไปรษณีย์");
  define("_PROFILE_DETAIL_TEXT_10", "ที่อยู่");
  define("_PROFILE_DETAIL_TEXT_11", "แก้ไขที่อยู่สำหรับการรับของรางวัล");
  define("_PROFILE_DETAIL_TEXT_12", "ของรางวัลทั้งหมด");
  define("_PROFILE_DETAIL_TEXT_13", "แก้ไขที่อยู่การรับรางวัล");
  define("_PROFILE_DESCRIPTION", "ทางบริษัทจะจัดส่งของรางวัลไปตามที่อยู่ที่ลูกค้าระบุมาในลอยัลตี้ โบนัส โปรแกรมเท่านั้น
                                กรุณาตรวจสอบที่อยู่สำหรับการจัดส่งของคุณ บริษัทจะไม่ขอรับผิดชอบในการชดใช้ค่าเสียหายใดๆ ที่เกิดขึ้นเนื่องมาจากการระบุข้อมูลที่อยู่ในการรับของรางวัลผิด");
  define("_FORM_ADDRESS_HEAD", "ที่อยู่สำหรับการรับของรางวัล");
  define("_UPDATE_PROFILE_TEXT_1", "ยืนยันข้อมูลที่อยู่สำเร็จ");
  define("_UPDATE_PROFILE_TEXT_2", "การจัดส่งของรางวัลจะยืนยันตามที่อยู่ที่ของคุณเท่านั้น");
  define("_UPDATE_PROFILE_TEXT_3", "บริษัทจะไม่ขอรับผิดชอบในการชดใช้ค่าเสียหายใดๆ ที่เกิดขึ้นเนื่องมาจากการระบุข้อมูลที่อยู่ในการรับของรางวัลผิด");
  define("_UPDATE_PROFILE_TEXT_4", "แลกของรางวัล");
  define("_UPDATE_PROFILE_TEXT_7", "ยืนยันข้อมูลสำเร็จ");

  // แลกของรางวัล //
  define("_REWARDTIMELINE_TEXT_1", "รวมรางวัลสถานะ GO Trader");
  define("_REWARDTIMELINE_TEXT_2", "รวมรางวัลสถานะ Advance Trader");
  define("_REWARDTIMELINE_TEXT_3", "รวมรางวัลสถานะ Expert Trader");
  define("_REWARDTIMELINE_TEXT_4", "รวมรางวัลสถานะ Master Trader");
  define("_REWARD_RANK_0", "รางวัลสถานะ");
  define("_REWARD_RANK_1", "GO Trader");
  define("_REWARD_RANK_2", "Advance Trader");
  define("_REWARD_RANK_3", "Expert Trader");
  define("_REWARD_RANK_4", "Master Trader");
  define("_REWARD_RANK_4_1", "600,000 - 3,600,000 Point");
  define("_REWARD_RANK_4_2", "4,200,000 - 30,000,000 Point");


  // รายละเอียดของรางวัล //
  define("_REWARD_DETAIL_TEXT_1", 'รางวัลสถานะ');
  define("_REWARD_DETAIL_TEXT_2", 'คะแนนสะสม');
  define("_REWARD_DETAIL_TEXT_3", '" คะแนนของคุณไม่เพียงพอต่อการรับของรางวัล "');
  define("_REWARD_DETAIL_TEXT_4", '" คุณได้รับของรางวัล Loyalty Program นี้แล้ว "');
  define("_REWARD_DETAIL_TEXT_5", '" กดเพื่อแลกรับของรางวัลของคุณได้ทันที  "');
  define("_REWARD_BUTTON_TEXT_1", 'ของรางวัลรวมทั้งหมด');
  define("_REWARD_BUTTON_TEXT_2", 'แลกรับของรางวัล');
  define("_REWARD_MODAL_TEXT_1", 'ยืนยันการแลกรับของรางวัล Loyalty Program');
  define("_REWARD_MODAL_TEXT_2", 'ยืนยันการรับของรางวัล');
  define("_REWARD_MODAL_TEXT_3", 'คะแนนสะสม');
  define("_REWARD_MODAL_TEXT_4", 'Point');
  define("_REWARD_MODAL_TEXT_5", 'ปิด');
  define("_REWARD_MODAL_TEXT_6", 'ยืนยันการทำรายการ');
  define("_REWARD_SUCCESS_TEXT_1", 'ขอแสดงความยินดี');
  define("_REWARD_SUCCESS_TEXT_2", 'คุณได้ทำการยืนยันรับของรางวัลเรียบร้อยแล้ว');
  define("_REWARD_SUCCESS_TEXT_3", 'สามารถสอบถามข้อมูลเพิ่มเติมได้ทุกช่องทาง หรือ Line : @GOFX');
  define("_REWARD_SUCCESS_TEXT_4", 'การแลกรับของรางวัลลอยัลตี้ จะใช้เวลาดำเนินการภายใน 7 วัน หลังจากกดยืนยันการรับของรางวัล
                                    คุณสามารถตรวจสอบสถานะของรางวัลได้ ในเมนู "ประวัติการแลก"');
  define("_REWARD_SUCCESS_TEXT_5", 'ขอบคุณ');  
  define("_REWARD_SUCCESS_TEXT_6", 'คุณสามารถตรวจสอบสถานะของรางวัลได้ ในเมนู "ประวัติการแลก" สามารถสอบถามข้อมูลเพิ่มเติมได้ทุกช่องทาง หรือ Line : @GOFX');
  define("_REWARD_SUCCESS_TEXT_7", 'ประวัติการแลก');
  define("_REWARD_LOGIN_TEXT_1", 'กรุณาเข้าสู่ระบบ GOFX ของคุณ');
  define("_REWARD_LOGIN_TEXT_2", 'กรุณาเข้าสู่ระบบ เพื่อนำคะแนน<br class="mb-ver">สะสมลอยัลตี้แลกรับของรางวัลสุดพิเศษ');
  define("_REWARD_LOGIN_TEXT_3", 'โปรโมชั่นสุดพิเศษเพื่อตอบแทนเทรดเดอร์ ผู้ใช้บริการของ GOFX.com ยกระดับการลงทุนของคุณให้คุ้มค่ายิ่งขึ้น');
  define("_REWARD_LOGIN_TEXT_4", 'เข้าสู่ระบบ');
  define("_REWARD_LOGIN_TEXT_5", 'กรุณาเข้าสู่ระบบ GOFX');
  // define("_REWARD_LOGIN_LINK", 'https://secure.gofx.com/th/sign-in.html');
  define("_REWARD_LOGIN_LINK", 'login');
  define("_REWARD_ADDRESS_TEXT_1", 'กรุณาระบุข้อมูลที่อยู่ของคุณ');
  define("_REWARD_ADDRESS_TEXT_2", 'ระบุข้อมูลที่อยู่สำหรับจัดส่งของรางวัลลอยัลตี้โปรแกรม');
  define("_REWARD_ADDRESS_TEXT_3", 'การจัดส่งของรางวัลจะยืนยันตามที่อยู่ที่ระบุในลอยัลตี้ โบนัส โปรแกรม ของทาง GOFX.com เท่านั้น กรุณาตรวจสอบความถูกต้องของที่อยู่ของคุณ');
  define("_REWARD_ADDRESS_TEXT_4", 'เพิ่มข้อมูลที่อยู่ของฉัน');
  
  // ประวัติการแลก //
  define("_HISTORY_HEAD", 'ประวัติการแลก');
  define("_HISTORY_SELECT_TEXT_1", 'วันที่เริ่มต้น');
  define("_HISTORY_SELECT_TEXT_2", 'วันที่สิ้นสุด');
  define("_HISTORY_SELECT_BUTTON_1", 'Apply');
  define("_HISTORY_SELECT_BUTTON_2", 'Cancel');
  define("_HISTORY_BOX_TEXT_1", 'คะแนนสะสม');
  define("_HISTORY_BOX_TEXT_2", 'Point');
  define("_HISTORY_BOX_TEXT_3", 'แลกรางวัลเมื่อวันที่');
  define("_HISTORY_BOX_TEXT_4", 'วันที่');
  define("_HISTORY_RESET_TITLE", 'รีเซทคะแนนและล็อตสะสมเป็นค่าเริ่มต้นของสถานะปัจจุบัน');
  define("_HISTORY_RESET_DESC", 'เทรดเดอร์ไม่มีการเคลื่อนไหวบัญชีเป็นระยะเวลา 6 เดือน คะแนนและล็อตสะสมของเทรดเดอร์จะถูกรีเซท ตามเงื่อนไขของลอยัลตี้โบนัสโปรแกรม');
  define("_HISTORY_CUSTOM_LOT_TITLE", 'คะแนนและล็อตสะสมของเทรดเดอร์ได้ถูกอัปเดต');
  define("_HISTORY_CUSTOM_LOT_DESC", 'ทางบริษัทได้มีการอัปเดตคะแนนและล็อตสะสมของเทรดเดอร์ให้มีความถูกต้องตามระบบ สามารถสอบถามข้อมูล เพิ่มเติมได้ทุกช่องทาง หรือ Line : @GOFX');

  // ข้อตกลงและเงื่อนไข //
  define("_TERM_AND_CONDITIONS_HEAD_1", "ข้อตกลงและเงื่อนไข");
  define("_TERM_AND_CONDITIONS_HEAD_2", "โปรโมชั่นสุดพิเศษเพื่อตอบแทนเทรดเดอร์ ผู้ใช้บริการของ GOFX.com ยกระดับการลงทุนของคุณให้คุ้มค่ายิ่งขึ้น รับรางวัลมากมายแบบไม่ต้องรอลุ้น
                                        ไม่ว่าจะเป็น Gadget สุดทันสมัย ไปจนถึงรถยนต์สุดหรูหลากหลายแบรนด์เช่น Maserati, Porshe, คูปองเงินสด และของรางวัลอีกมากมายที่เราคัดสรรมาเพื่อคุณโดยเฉพาะ");
  define("_TERM_AND_CONDITIONS_HEAD_3", "กรุณาอ่านข้อกำหนดและเงื่อนไขต่างๆ ดังต่อไปนี้อย่างละเอียด โดยในการเข้าร่วมกิจกรรม Loyalty Program ของทาง GOFX.com
                                        ถือว่าท่านตกลงที่จะผูกพันตามข้อกำหนดและเงื่อนไขการใช้บริการนี้");
  define("_TERM_AND_CONDITIONS_DETAIL_1", "ก.	เงื่อนไขและข้อตกลงของลอยัลตี้โบนัสโปรแกรม");
  define("_TERM_AND_CONDITIONS_DETAIL_2", "1. GOFX.com อยู่ภายใต้การดำเนินงานของ GOFX Limited ซึ่งเป็นผู้ค้าหลักทรัพย์ที่ได้รับจากการจดทะเบียน หมายเลข 25865BC2020
                                          มีที่อยู่ที่ใช้จดทะเบียนคือ Suite 305, Griffith Corporate Centre, P.O.Box 1510, Beachmont, Kingstown, St. Vincent and the Grenadines.");
  define("_TERM_AND_CONDITIONS_DETAIL_3", "2. ลอยัลตี้โบนัสโปรแกรม คือโปรโมชั่นสุดพิเศษเพื่อตอบแทนเทรดเดอร์ ผู้ใช้บริการของ GOFX.com โดยท่านจะได้รับของรางวัลอันมีค่ามากมายตั้งแต่
                                          GOFX Premium Product, คอร์สสอนการเทรด จากสถาบัน GOTA Go Trade Academy, Gadget สุดทันสมัย ไปจนถึงรถยนต์สุดหรูหลากหลายแบรนด์เช่น 
                                          Maserati, Porshe เป็นต้น เพียงทำการซื้อ-ขาย และสะสมแต้มให้ครบ ท่านก็สามารถนำคะแนนที่ท่านสะสมไปแลกเป็นของรางวัลต่าง ๆ ที่ต้องการได้แล้ว");
  define("_TERM_AND_CONDITIONS_DETAIL_4", "ข. คำศัพท์");
  define("_TERM_AND_CONDITIONS_DETAIL_5", "1. Go Point คือ หน่วยคะแนนที่ได้จากการทำรายการ ซื้อ-ขาย ภายใต้ลอยัลตี้โบนัสโปรแกรม </br>
                                          2. GO Trader คือ สถานะระดับที่ 1 </br>
                                          3. Advance Trader คือ สถานะระดับที่ 2 </br>
                                          4. Expert Trader คือ สถานะระดับที่ 3 </br>
                                          5. Master Trader คือ สถานะระดับที่ 4 </br>
                                          ");
  define("_TERM_AND_CONDITIONS_ARTICLE", "ค. บทบัญญัติทั่วไป");
  define("_TERM_AND_CONDITIONS_ARTICLE_1", "1. สถานะลอยัลตี้โบนัสโปรแกรม คือ ระดับของเทรดเดอร์ที่เข้าร่วมลอยัลตี้โบนัสโปรแกรม โดยการเลื่อนระดับจะขึ้นอยู่กับการสะสมคะแนน
                                            (GO Point) ซึ่งจะถูกคำนวณจากปริมาณการซื้อ-ขาย (Lot) ของเทรดเดอร์ ");
  define("_TERM_AND_CONDITIONS_ARTICLE_2", "2. ในแต่ละสถานะลอยัลตี้โบนัสโปรแกรม จะได้รับคะแนน GO point ที่แตกต่างกันดังนี้");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_1", "2.1 ตารางสถานะลอยัลตี้โบนัสต่อคะแนนที่ได้รับ");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_1_DETAIL_1", "ตารางสถานะลอยัลตี้โบนัสต่อคะแนนที่ได้รับ");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_2", "2.2 ตัวอย่างเช่น เมื่อเทรดเดอร์อยู่ในสถานะ Go Trader และซื้อ-ขาย 10 ล็อต เทรดเดอร์จะได้รับ (10 ล็อต X 10 GO point) = 100
                                              GO point และเมื่อเทรดเดอร์ได้รับการเลื่อนขั้นเป็น Advance Trader และซื้อ-ขาย10 ล็อต เทรดเดอร์จะได้รับ
                                              (10 ล็อต X 12 GO point) = 120 GO point เป็นต้น ");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_3", "2.3 กรณี ‘บัญชี Low Spread’ จำนวนล็อตของลูกค้าจะถูก หารด้วย 10 กล่าวคือ 1 ล็อต ของบัญชี Low Spread จะได้รับคะแนนตามตารางดังต่อไปนี้");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_3_DETAIL_1", " ตารางสถานะลอยัลตี้โบนัสต่อคะแนนที่ได้รับ : สำหรับบัญชี Low Spread");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_3_DETAIL_2", "จะเริ่มนับโบนัสลอยัลตี้ของบัญชี Low spread ตั้งแต่วันที่ 8 พฤศจิกายน 2566 เป็นต้นไป");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_4", "2.4 กรณี ‘บัญชีมินิ’  จำนวนล็อตของลูกค้าจะถูก หารด้วย 100 เพื่อเปลี่ยน USC เป็นหน่วย  USD ก่อนถูกคำนวณเป็นคะแนนในลอยัลตี้โปรแกรม 
  กล่าวคือ 1 ล็อต ของบัญชีมินิ จะได้รับคะแนนตามตารางดังต่อไปนี้");
define("_TERM_AND_CONDITIONS_ARTICLE_2_4_DETAIL_1", " ตารางสถานะลอยัลตี้โบนัสต่อคะแนนที่ได้รับ : สำหรับบัญชี mini");
  define("_TERM_AND_CONDITIONS_ARTICLE_3", "3. เทรดเดอร์ของ GOFX.com ที่เปิดบัญชีจริง ทุกท่านจะมีสิทธิ์เข้าร่วมลอยัลตี้โบนัสโปรแกรม โดยอัตโนมัติไม่ต้องมีการลงทะเบียนใดๆเพิ่มเติม
                                             โดยเทรดเดอร์ทุกท่านจะได้สถานะ Go Trader เป็นสถานะเริ่มต้น  ยกเว้นบัญชี BONUS $30 และ DEMO");
  define("_TERM_AND_CONDITIONS_ARTICLE_4", "4.	คะแนนสะสมและล็อตสะสมลอยัลตี้ของเทรดเดอร์จะถูก ‘รีเซท’ เป็น ‘คะแนนและล็อตสะสมเริ่มต้นของสถานะปัจจุบัน’ หากไม่มีการเคลื่อนไหว
                                           บัญชีเป็นระยะเวลา 6 เดือน และสถานะของเทรดเดอร์จะไม่ถูก ‘รีเซท’");
  define("_TERM_AND_CONDITIONS_ARTICLE_5", "5.	ในกรณีที่บัญชีของเทรดเดอร์ถูก ‘รีเซทคะแนนสะสมและล็อตสะสมลอยัลตี้’  จะไม่สามารถทำการกู้คะแนนที่ถูกรีเซทไปได้ แต่เทรดเดอร์สามารถ
                                            เริ่มสะสมคะแนนใหม่ตามเงื่อนไขที่บริษัทฯกำหนด หากเริ่มการซื้อ-ขายใหม่แล้ว เทรดเดอร์จะไม่สามารถรับรางวัลซ้ำกับรางวัลที่เคยแลกก่อนถูก
                                            รีเซทคะแนนได้ เพื่อทำการรับรางวัลต่อ เทรดเดอร์จะต้องซื้อ-ขาย และสะสมคะแนนให้ครบเทียบเท่าคะแนนล่าสุดก่อนถูกรีเซทคะแนน");
  define("_TERM_AND_CONDITIONS_ARTICLE_6", "6.	เทรดเดอร์ทุกท่านจะได้รับการยกระดับสถานะลอยัลตี้โบนัสโปรแกรม จากการสะสมจำนวนล็อตการซื้อ-ขาย หากเทรดเดอร์ท่านใดทำการ
                                            ซื้อ-ขายจำนวนมาก สถานะลอยัลตี้โบนัสโปรแกรม ก็จะสูงขึ้นไปด้วย และคะแนนที่ได้รับก็จะเพิ่มขึ้นเช่นกัน โดยคะแนนของจำนวนล็อตการเทรดจะถูกจ่ายไปยังบัญชีของเทรดเดอร์โดยอัตโนมัติ (ดูหลักการคำนวณแต้มในข้อ 2.1) ");
  define("_TERM_AND_CONDITIONS_ARTICLE_6_1", "6.1 คะแนน GO point ไม่สามารถทำการถ่ายโอนไปให้บัญชีประเภทอื่นภายใต้ชื่อเดียวกัน หรือบัญชีของบุคคลอื่นได้");
  define("_TERM_AND_CONDITIONS_ARTICLE_7", "7.	ข้อกำหนดการแลกของรางวัล แต้มของสถานะลอยัลตี้โบนัสโปรแกรม เป็นคะแนนที่เอาไว้แลกของรางวัลพิเศษสำหรับเทรดเดอร์ GOFX.com
                                           โดยวิธีการคำนวณคะแนนจะเป็นไปตามสูตร (ดูในข้อ 2.2) ซึ่งการแลกของรางวัลมีข้อกำหนด ดังนี้ ");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_1", "เทรดเดอร์สามารถแลกคะแนนเป็นของรางวัลต่างๆ ได้ตามสถานะลอยัลตี้โบนัสโปรแกรม ในปัจจุบันของท่าน โดยในการแลกของรางวัล
                                             เทรดเดอร์จะต้องมีคะแนน GO point ที่เพียงพอและทำครบตามเงื่อนไขที่กำหนด");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_2", "หากเทรดเดอร์ได้ทำการแลกของรางวัลตามสถานะลอยัลตี้โบนัสโปรแกรมที่ต้องการ คะแนนของท่านจะไม่ถูกลดลงและคะแนนจะถูก
                                             เพิ่มขึ้นเรื่อยๆจากจำนวนล็อตที่เทรดเดอร์ซื้อ-ขาย เทรดเดอร์สามารถเพลิดเพลินไปกับการปลดล็อคของรางวัลสุดพิเศษไปเรื่อยๆ");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_3", "เทรดเดอร์สามารถดำเนินการขอแลกของรางวัลได้ใน Member Area โดยทำการกดเลือกของรางวัลที่ท่านต้องการและกดดำเนินการขอแลกของรางวัล 
                                              จากนั้นท่านจะได้รับการติดต่อจากเจ้าหน้าที่ภายใน 1 วันเพื่อแจ้งรายละเอียดของการรับรางวัลต่อไป ");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_4", 'ในการ “แลกคะแนนเป็นเงิน” เทรดเดอร์สามารถกดรับคูปองเงินได้ในแคตตาล็อกของรางวัล โดยคูปองนี้สามารถเปลี่ยนเป็นเงินในระบบ
                                              และสามารถถอนออกได้');
  define("_TERM_AND_CONDITIONS_ARTICLE_7_5", "หลังจากได้รับการอนุมัติคำขอรับรางวัลแล้ว GOFX.com จะกระทำการมอบของรางวัลภายใน 30 วันทำการ ในกรณีที่ของรางวัลมีการ
                                              ขาดตลาด การดำเนินการอาจมีความล่าช้าออกไป โดยจะมีการแจ้งสถานการณ์ดำเนินการให้ทราบอยู่เสมอ");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_6", "ค่าใช้จ่ายเพิ่มเติมในการดำเนินการทั้งหมด เช่น ค่าขนส่งสินค้า ภาษี และอื่น ๆ ผู้รับรางวัลจะต้องรับผิดชอบส่วนต่างนั้นเองทั้งหมด 
                                              โดยต้องทำการชำระภาษีหัก ณ ที่จ่าย 5% ก่อนรับของรางวัล");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_7", "GOFX.com ไม่มีส่วนรับผิดชอบต่อคุณภาพและการบริการหลังการขายใด ๆ ของสินค้าต่าง ๆ ที่มอบให้แก่ผู้รับรางวัล");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_8", "GOFX.com ขอสงวนสิทธิ์ในการใช้ภาพถ่าย และวีดีโอของเทรดเดอร์กิจกรรมเพื่อการประชาสัมพันธ์หรือการตลาด");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_9", "หากบริษัทไม่สามารถติดต่อเทรดเดอร์ได้ หรือเทรดเดอร์ไม่ตอบรับการติดต่อภายใน 28 วัน ของรางวัลนั้นจะถูกยกเลิก");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_10", "บริษัทจะมีกำหนดการอัปเดตของรางวัล ทุก 6 เดือน อาจะเป็นการอัปเดตสินค้าทั้งหมด หรือแค่บางชิ้นเท่านั้น");
  define("_TERM_AND_CONDITIONS_ARTICLE_8", "8. บทบัญญัติข้อสุดท้าย");
  define("_TERM_AND_CONDITIONS_ARTICLE_8_1", "GOFX.com ขอสงวนสิทธิ์ในการเปลี่ยนแปลงหรือปรับปรุงข้อกำหนดและเงื่อนไขโดยไม่ต้องแจ้งให้ทราบล่วงหน้า ");
  define("_TERM_AND_CONDITIONS_ARTICLE_8_2", "GOFX.com ขอสงวนสิทธิ์ในการยกเลิกการให้รางวัลแก่เทรดเดอร์ที่มีที่มาของแต้มที่น่าสงสัยว่าอาจจะมาจากพฤติกรรมที่น่าสงสัย
                                              ในกรณีที่ข้อกล่าวหาได้รับการพิสูจน์ว่ามีความโปร่งใส คะแนนของการซื้อ-ขายทั้งหมดที่ถูกระงับจะถูกจ่ายคืนให้กับบัญชีของเทรดเดอร์โดยอัตโนมัติ");
  define("_TERM_AND_CONDITIONS_ARTICLE_8_3", "หากเทรดเดอร์ไม่ต้องการเข้าร่วมในลอยัลตี้โบนัสโปรแกรม เทรดเดอร์สามารถทำได้โดยแจ้งมาที่ศูนย์บริการเทรดเดอร์ของ GOFX.com ทาง GOFX.com
                                              จะดำเนินการตามคำร้องขอของท่านและแจ้งให้ทราบต่อไป");
  define("_TERM_AND_CONDITIONS_ARTICLE_8_4", "การเข้าร่วมลอยัลตี้โบนัสโปรแกรม คือการที่เทรดเดอร์ยินยอมเงื่อนไขและข้อตกลงของ ลอยัลตี้โบนัสโปรแกรม");

  // ติดต่อเรา //
  define("_CONTACT_HEAD", 'ติดต่อเรา');
  define("_CONTACT_HEADER_TITLE", '“ เราเชื่อมผู้คนกับเงินเข้าหากันด้วยวิธีที่เป็นมิตรที่สุด ”');
  define("_CONTACT_HEADER_TEXT1", "GOFX (International) Limited");
  define("_CONTACT_HEADER_TEXT2", "เชื่อว่าการเงินเป็นเรื่องของทุกคน เราจึงสนับสนุนและจัดกิจกรรมส่งเสริมการให้ความรู้
                                  ทางด้านเทคนิคการวิเคราะห์การลงทุนและเทคนิคการวางแผนการเงินส่วนบุคคล รวมถึงพัฒนาระบบการซื้อขาย ให้สอด
                                  คล้องกับความต้องการและความพึงพอใจทางด้านคําสั่งซื้อขายที่เหมาะสม กับความต้องการของเทรดเดอร์ และนักลงทุน
                                  ในแต่ละพื้นที่การใช้งาน โดยมุ่งเน้นการทํากิจกรรมการตลาด และโปรโมชั่นส่งเสริมการขาย ในภูมิภาคเอเชียตะวันออกเฉียงใต้ 
                                  (Southeast Asia) โดยให้ความสําคัญไปยังกลุ่มประเทศที่มีจํานวนผู้สนใจ ใช้บริการเทรดดิ้ง แพลตฟอร์มในการซื้อขายเก็งกําไรในตลาดทุนมากที่สุด 
                                  ได้แก่ สาธารณรัฐสิงคโปร์ สาธารณรัฐสังคมนิยมเวียดนาม ราชอาณาจักรไทยและสาธารณรัฐประชาธิปไตยประชาชนลาว");
  define("_CONTACT_SOCIAL_1", '02-026-6559');
  define("_CONTACT_SOCIAL_2", '@GOFX');
  define("_CONTACT_SOCIAL_3", 'support@gofx.com');
  define("_CONTACT_SOCIAL_4", 'GOFXthailand');
  define("_CONTACT_FOOTER_TEXT1", "GOFX.COM อยู่ภายใต้การดำเนินงานของ GOFX Limited ซึ่งเป็นผู้ค้าหลักทรัพย์ที่ได้รับจากการจดทะเบียน หมายเลข 25865BC2020</br>
                                  ซึ่งมีที่อยู่ที่ใช้จดทะเบียนคือ Suite 305, Griffith Corporate Centre, P.O.Box 1510, Beachmont, Kingstown, St. Vincent and the Grenadines.");
  define("_CONTACT_FOOTER_TEXT2", "บริษัท GOFX Limited เป็นเจ้าของลิขสิทธิ์ ในข้อมูลที่แสดง รูปภาพ และรูปแบบการแสดงผล ตามที่ปรากฎ ในเว็บไซต์ทั้งหมด
                                  ยกเว้นจะมีการระบุอย่างชัดเจนเป็นอื่น ห้ามมิให้ผู้ใดทำการคัดลอก ทำซ้ำ มีสำเนา สำรองไว้ ทำเลียนแบบ ทำเหมือน ดัดแปลง ทำเพิ่ม ซึ่งนำไปเผยแพร่ด้วยวัตถุประสงค์อื่นใด
                                  นอกจากได้รับอนุญาตจากบริษัทเป็นลายลักษณ์อักษร");
  define("_CONTACT_FOOTER_TEXT3", "คำเตือนความเสี่ยงทั่วไป : สัญญาส่วนต่าง (Contract for difference) หรือ CFD เป็นผลิตภัณฑ์ที่ใช้อัตราทด (Leverage)
                                  การซื้อขาย CFD จึงมีความเสี่ยงสูง ซึ่งอาจไม่เหมาะกับนักลงทุนทุกคน มูลค่าการลงทุนอาจเพิ่มขึ้นหรือลดลงได้ และนักลงทุนอาจสูญเสียเงินลงทุนทั้งหมด ไม่ว่าในสถานการณ์ใด
                                  บริษัทและหน่วยงานที่เกี่ยวข้องจะไม่รับผิดชอบต่อบุคคลหรือหน่วยงานใด หากเกิดการขาดทุนหรือสูญเสียเงินลงทุนทั้งหมดหรือบางส่วน ซึ่งเป็นผลจากการทำคำสั่งซื้อขายหรือเกี่ยวข้องกับธุรกรรมใดที่เกี่ยวเนื่องกับ CFD");
  define("_CONTACT_FOOTER_TEXT4", "คำเตือน : การลงทุนมีความเสี่ยง ผู้ลงทุนควรศึกษาข้อมูลก่อนการตัดสินใจลงทุน");
  define("_CONTACT_FOOTER_TEXT5", "Risk warning : Our services involve a significant risk and can result in the loss of your invested capital.");
?>
