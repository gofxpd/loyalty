<?php
  // nav bar //
  define("_SIGNIN_LINK", "https://secure.gofx.com/sign-in.html");
  // define("_SIGNIN_LINK", "login");
  define("_REGISTER_LINK", "https://secure.gofx.com/newAccount.html");
  define("_SIGNIN", "Sign in");
  define("_REGISTER", "Register");
  define("_SIGNOUT", "Sign out");
  define("_PROFILE", "จัดการข้อมูลส่วนตัว");

  // Main Menu //
  define("_MAINMENU_INDEX", "HOME");
  define("_MAINMENU_REWARDS", "REWARDS");
  define("_MAINMENU_HISTORY", "HISTORY");
  define("_MAINMENU_TERM", "TERM & CONDITIONS");
  define("_MAINMENU_CONTACT", "CONTACT US");
  define("_LOGOUT", "LOGOUT");
  define("_Flag", "United-states.png");

  // Mobile Menu //
  define("_MOBILEMENU_INDEX", "Home");
  define("_MOBILEMENU_REWARDS", "Rewards");
  define("_MOBILEMENU_ACCOUNT", "Account");
  define("_MOBILEMENU_TERMS", "Term & Conditions");
  define("_MOBILEMENU_NAV", "Menu");

  // Banner desktop, tablet //
  define("_BANNER_HEADTEXT1", "LOYALTY PROGRAM");
  define("_BANNER_HEADTEXT2", "สถานะลอยัลตี้ของคุณ");
  define("_BANNER_HEADTEXT3", "Reward points");
  define("_BANNER_TEXT1", " GOFX.com is delighted to offer special promotions to reward our valued traders. Boost your investments,
                          win many prizes, and claim it right away. Our prizes include a wide variety of choices which includes cutting-edge gadgets,
                          luxury car brands such as Maserati and Porsche, cash coupons, and many more prizes meticulously selected just for you.");
  define("_BANNER_TEXT2", "Master Trader");
  define("_BANNER_TEXT3", "Loyalty reward points :");
  define("_BANNER_TEXT4", "Loyalty reward lots :");

  // Banner mobile //
  define("_BANNER_MB_HEADTEXT3", "Reward points");
  define("_BANNER_MB_TEXT1", "GOFX.com is delighted to offer special promotions to reward our valued traders. Boost your investments,
                          win many prizes, and many more prizes meticulously selected just for you.");
  define("_BANNER_MB_TEXT3", "GO Point : ");
  define("_BANNER_MB_TEXT4", "Lots : ");

  // Footer //
  define("_FOOTER_MAIN1", "Account Types");
  define("_FOOTER_MAIN2", "Trading");
  define("_FOOTER_MAIN3", "Trading Conditions");
  define("_FOOTER_MAIN4", "MT4");
  define("_FOOTER_MAIN5", "Contact Us");

  define("_FOOTER_MAIN1_1", "Type of Account");
  define("_FOOTER_MAIN2_1", "Forex");
  define("_FOOTER_MAIN2_2", "Indexes CFD");
  define("_FOOTER_MAIN2_3", "Energy CFDs");
  define("_FOOTER_MAIN2_4", "Precious Metals CFDs");
  define("_FOOTER_MAIN2_5", "Stock CFDs");
  define("_FOOTER_MAIN2_6", "Cryptocurrency");
  define("_FOOTER_MAIN3_1", "Privacy Policy");
  define("_FOOTER_MAIN3_2", "Terms and Conditions");
  define("_FOOTER_MAIN3_3", "Trading Time");
  define("_FOOTER_MAIN3_4", "คำถามที่พบบ่อย");
  define("_FOOTER_MAIN4_1", "MT4 for PC");
  define("_FOOTER_MAIN4_2", "MT4 for iPhone");
  define("_FOOTER_MAIN4_3", "MT4 for Android");
  define("_FOOTER_MAIN5_1", "Contact Us");

  // หนเ้าแรก //
  define("_REWARD_TEXT_1", "Reward points");
  define("_REWARD_TEXT_2", "Loyalty status");
  define("_REWARD_TEXT_3", "Point");
  define("_REWARD_TEXT_4", "Lot : GO Point");
  define("_REWARD_LOT_GO", "1 Lot = 10 Point");
  define("_REWARD_POINT_GO", "0 - 1.8K Point");
  define("_REWARD_LOT_ADVANCE", "1 Lot = 12 Point");
  define("_REWARD_POINT_ADVANCE", "2.8K - 105K Point");
  define("_REWARD_LOT_EXPERT", "1 Lot = 15 Point");
  define("_REWARD_POINT_EXPERT", "116K - 540K Point");
  define("_REWARD_LOT_MASTER", "1 Lot = 20 Point");
  define("_REWARD_POINT_MASTER", "585K - 30M Point");
  define("_REWARDTIMELINE_TEXT", "รวมรางวัลสถานะ");
  define("_REWARDTIMELINE_TEXT_GO", "GO Trader's rewards");
  define("_REWARDTIMELINE_TEXT_ADVANCE", "Advance Trader's rewards");
  define("_REWARDTIMELINE_TEXT_EXPERT", "Expert Trader's rewards");
  define("_REWARDTIMELINE_TEXT_MASTER", "Master Trader’s rewards");
  define("_REWARDTIMELINE_Go", "Begin your first steps in your trading journey with premium rewards from GOFX. We offer many rewards to repay your commitment
                                to trading. Breakthrough the wall and clear all challenges to unlock the next level. We are here to motivate you along your trading journey. ");
  define("_REWARDTIMELINE_Advance", "Congratulations on reaching the 2nd level! We celebrate your success by selecting special rewards to pamper all types of
                                    traders including health and beauty lovers, gadget geeks, or travel enthusiast. You can also redeem your points for cash coupons.
                                    Time to unlock the next level and enjoy rewards at every stage with GOFX.");
  define("_REWARDTIMELINE_Expert", "Step up your game and prepare to soar up high. We are overjoyed to announce that on this level you will definitely enjoy the next
                                    level of perfection through our awesome rewards and prizes. Expert Traders will be rewarded with a variety of luxurious prizes for
                                    all ages, ranging from a bedazzling Channel Necklace to the latest iMAC and many more.");
  define("_REWARDTIMELINE_Master", "What an outstanding journey! GOFX congratulates you for unlocking the highest level. A Master Trader is an exclusive status and a
                                    symbol of your hard work and dedication to trading with GOFX. You can have various privileges and continue to unlock exclusive
                                    luxurious rewards like a luxury Private jet trip, a Porsche 911, or Maserati. ");
  define("_REWARD_BUTTON", "See award’s details");
  define("_REWARDTIMELINE_BUTTON", "Redeem Rewards");
  define("_LOYALTY_VIDEO", "Award-winning activities ");
  define("_LOYALTY_VIDEO_TEXT", "GOFX ให้คุณสัมผัสกับประสบการณ์ใหม่ รับความพิเศษเหนือระดับกับ LOYALTY PROGRAM");
  define("_LOYALTY_SLIDER", "ภาพกิจกรรมรับรางวัล");
  define("_LOYALTY_PROMOTION", "สิทธิประโยชน์ & โปรโมชั่น อีกมากมาย");
  define("_LOYALTY_ABOUTUS", "About Us");
  define("_LOYALTY_ABOUTUS_TEXT", 'GOFX Limited  คือ "เทรดดิ้งแพลตฟอร์ม" ที่ก่อตั้งขึ้นโดยทีมงานผู้บริหารที่มีความเชี่ยวชาญในด้านเทคโนโลยีทางการเงินและกลุ่มเทรดเดอร์ผู้มี
                                  ประสบการณ์ระดับมืออาชีพในตลาดซื้อขายสากล เราจึงเข้าใจนักลงทุนมากที่สุด และGOFX Limited ยังเชื่อว่าการเงินเป็นเรื่องของทุกคน
                                  เราจึงมุ่งมั่นออกแบบเทรดดิ้งแพลตฟอร์ม และกิจกรรมสนับสนุน ที่เหมาะสมกับความต้องการของเทรดเดอร์ในแต่ละพื้นที่การใช้งานอย่างเข้าใจ
                                  เราพร้อมจะอยู่เคียงข้างคุณตลอดเส้นทางการลงทุน');


  // หนเ้าแรก mobile //
  define("_REWARDTIMELINE_MB_TEXT", "รางวัลสถานะ");
  define("_REWARDTIMELINE_MB_Go", "ก้าวแรกของการเทรด เริ่มต้นเส้นทางของคุณด้วยของรางวัลสุดพรีเมี่ยมจาก GOFX และของรางวัลอื่นๆอีกมากมาย");
  define("_REWARDTIMELINE_MB_Advance", "รางวัลฉลองก้าวที่ 2 ของคุณ ยกระดับความท้าทายของคุณ ทาง GOFX ได้คัดสรรของรางวัลสุดพิเศษ เอาใจเทรดเดอร์ทุกสาย");
  define("_REWARDTIMELINE_MB_Expert", "คุณจะสามารถยกระดับความสมบูรณ์แบบขึ้นไปอีก GOFX ใส่ใจคุณทุกย่างก้าวและขอมอบรางวัลตอบแทนให้คุณอย่างคุ้มค่า");
  define("_REWARDTIMELINE_MB_Master", "ยินดีด้วย! คุณได้ปลดล็อคมาจนถึงระดับสูงสุด Master trader คือสถานะสุด Exclusive ที่คุณจะได้รับสิทธิพิเศษต่างๆมากมาย");

  // ข้อมูลส่วนตัว //
  define("_PROFILE_DETAIL_HEAD", "ข้อมูลผู้ถือบัญชี");
  define("_PROFILE_DETAIL_TEXT_1", "ผู้ถือบัญชี");
  define("_PROFILE_DETAIL_TEXT_2", "เบอร์โทรศัพท์");
  define("_PROFILE_DETAIL_TEXT_3", "อีเมล");
  define("_PROFILE_DETAIL_TEXT_4", "ประเทศ");
  define("_PROFILE_DETAIL_TEXT_5", "ที่อยู่รับของรางวัล");
  define("_PROFILE_DETAIL_TEXT_6", "จังหวัด");
  define("_PROFILE_DETAIL_TEXT_7", "เขต/อำเภอ");
  define("_PROFILE_DETAIL_TEXT_8", "แขวง/ตำบล");
  define("_PROFILE_DETAIL_TEXT_9", "รหัสไปรษณีย์");
  define("_PROFILE_DETAIL_TEXT_10", "ที่อยู่");
  define("_PROFILE_DETAIL_TEXT_11", "แก้ไขที่อยู่สำหรับการรับของรางวัล");
  define("_PROFILE_DETAIL_TEXT_12", "ของรางวัลทั้งหมด");
  define("_PROFILE_DESCRIPTION", "ทางบริษัทจะจัดส่งของรางวัลไปตามที่อยู่ที่ลูกค้าระบุมาในลอยัลตี้ โบนัส โปรแกรมเท่านั้น
                                กรุณาตรวจสอบที่อยู่สำหรับการจัดส่งของคุณ บริษัทจะไม่ขอรับผิดชอบในการชดใช้ค่าเสียหายใดๆ ที่เกิดขึ้นเนื่องมาจากการระบุข้อมูลที่อยู่ในการรับของรางวัลผิด");
  define("_FORM_ADDRESS_HEAD", "ที่อยู่สำหรับการรับของรางวัล");
  define("_UPDATE_PROFILE_TEXT_1", "ยืนยันข้อมูลที่อยู่สำเร็จ");
  define("_UPDATE_PROFILE_TEXT_2", "การจัดส่งของรางวัลจะยืนยันตามที่อยู่ที่ของคุณเท่านั้น");
  define("_UPDATE_PROFILE_TEXT_3", "บริษัทจะไม่ขอรับผิดชอบในการชดใช้ค่าเสียหายใดๆ ที่เกิดขึ้นเนื่องมาจากการระบุข้อมูลที่อยู่ในการรับของรางวัลผิด");
  define("_UPDATE_PROFILE_TEXT_4", "แลกของรางวัล");

  // แลกของรางวัล //
  define("_REWARDTIMELINE_TEXT_1", "GO Trader's rewards");
  define("_REWARDTIMELINE_TEXT_2", "Advance Trader's rewards");
  define("_REWARDTIMELINE_TEXT_3", "Expert Trader's rewards");
  define("_REWARDTIMELINE_TEXT_4", "Master Trader’s rewards");
  define("_REWARD_RANK_0", "รางวัลสถานะ");
  define("_REWARD_RANK_1", "GO Trader");
  define("_REWARD_RANK_2", "Advance Trader");
  define("_REWARD_RANK_3", "Expert Trader");
  define("_REWARD_RANK_4", "Master Trader");
  define("_REWARD_RANK_4_1", "600,000 - 3,600,000 Point");
  define("_REWARD_RANK_4_2", "4,200,000 - 30,000,000 Point");

  // รายละเอียดของรางวัล //
  define("_REWARD_DETAIL_TEXT_1", '');
  define("_REWARD_DETAIL_TEXT_2", 'Reward points');
  define("_REWARD_DETAIL_TEXT_3", '" คะแนนของคุณไม่เพียงพอต่อการรับของรางวัล "');
  define("_REWARD_DETAIL_TEXT_4", '" คุณได้รับของรางวัล Loyalty Program นี้แล้ว "');
  define("_REWARD_DETAIL_TEXT_5", '" กดเพื่อแลกรับของรางวัลของคุณได้ทันที  "');
  define("_REWARD_BUTTON_TEXT_1", 'ของรางวัลรวมทั้งหมด');
  define("_REWARD_BUTTON_TEXT_2", 'แลกรับของรางวัล');
  define("_REWARD_MODAL_TEXT_1", 'ยืนยันการแลกรับของรางวัล Loyalty Program');
  define("_REWARD_MODAL_TEXT_2", 'ยืนยันการรับของรางวัล');
  define("_REWARD_MODAL_TEXT_3", 'คะแนนสะสม');
  define("_REWARD_MODAL_TEXT_4", 'Point');
  define("_REWARD_MODAL_TEXT_5", 'ปิด');
  define("_REWARD_MODAL_TEXT_6", 'ยืนยันการทำรายการ');
  define("_REWARD_SUCCESS_TEXT_1", 'ขอแสดงความยินดี');
  define("_REWARD_SUCCESS_TEXT_2", 'คุณได้ทำการยืนยันรับของรางวัลเรียบร้อยแล้ว');
  define("_REWARD_SUCCESS_TEXT_3", 'สามารถสอบถามข้อมูลเพิ่มเติมได้ทุกช่องทาง หรือ Line : @GOFX');
  define("_REWARD_SUCCESS_TEXT_4", 'การแลกรับของรางวัลลอยัลตี้ จะใช้เวลาดำเนินการภายใน 7 วัน หลังจากกดยืนยันการรับของรางวัล
                                    คุณสามารถตรวจสอบสถานะของรางวัลได้ ในเมนู "ประวัติการแลก"');
  define("_REWARD_SUCCESS_TEXT_5", 'ขอบคุณ');  
  define("_REWARD_SUCCESS_TEXT_6", 'คุณสามารถตรวจสอบสถานะของรางวัลได้ ในเมนู "ประวัติการแลก" สามารถสอบถามข้อมูลเพิ่มเติมได้ทุกช่องทาง หรือ Line : @GOFX');
  define("_REWARD_SUCCESS_TEXT_7", 'ประวัติการแลก');
  define("_REWARD_LOGIN_TEXT_1", 'กรุณาเข้าสู่ระบบ GOFX ของคุณ');
  define("_REWARD_LOGIN_TEXT_2", 'กรุณาเข้าสู่ระบบ เพื่อนำคะแนน<br class="mb-ver">สะสมลอยัลตี้แลกรับของรางวัลสุดพิเศษ');
  define("_REWARD_LOGIN_TEXT_3", 'โปรโมชั่นสุดพิเศษเพื่อตอบแทนเทรดเดอร์ ผู้ใช้บริการของ GOFX.com ยกระดับการลงทุนของคุณให้คุ้มค่ายิ่งขึ้น');
  define("_REWARD_LOGIN_TEXT_4", 'เข้าสู่ระบบ');
  define("_REWARD_LOGIN_TEXT_5", 'กรุณาเข้าสู่ระบบ GOFX');
  // define("_REWARD_LOGIN_LINK", 'https://secure.gofx.com/th/sign-in.html');
  define("_REWARD_LOGIN_LINK", 'login');
  define("_REWARD_ADDRESS_TEXT_1", 'กรุณาระบุข้อมูลที่อยู่ของคุณ');
  define("_REWARD_ADDRESS_TEXT_2", 'ระบุข้อมูลที่อยู่สำหรับจัดส่งของรางวัล ลอยัลตี้ โปรแกรม');
  define("_REWARD_ADDRESS_TEXT_3", 'การจัดส่งของรางวัลจะยืนยันตามที่อยู่ที่ระบุในลอยัลตี้ โบนัส โปรแกรม ของทาง GOFX.com เท่านั้น กรุณาตรวจสอบความถูกต้องของที่อยู่ของคุณ');
  define("_REWARD_ADDRESS_TEXT_4", 'เพิ่มข้อมูลที่อยู่ของฉัน');

  // ประวัติการแลก //
  define("_HISTORY_HEAD", 'History');
  define("_HISTORY_SELECT_TEXT_1", 'Start');
  define("_HISTORY_SELECT_TEXT_2", 'End');
  define("_HISTORY_SELECT_BUTTON_1", 'Apply');
  define("_HISTORY_SELECT_BUTTON_2", 'Cancel');
  define("_HISTORY_BOX_TEXT_1", 'Reward points');
  define("_HISTORY_BOX_TEXT_2", 'Point');
  define("_HISTORY_BOX_TEXT_3", 'Redeem Reward');
  define("_HISTORY_BOX_TEXT_4", 'Date');
  define("_HISTORY_RESET_TITLE", 'รีเซทคะแนนและล็อตสะสมเป็นค่าเริ่มต้นของสถานะปัจจุบัน');
  define("_HISTORY_RESET_DESC", 'เทรดเดอร์ไม่มีการเคลื่อนไหวบัญชีเป็นระยะเวลา 6 เดือน คะแนนและล็อตสะสมของเทรดเดอร์จะถูกรีเซท ตามเงื่อนไขของลอยัลตี้ โบนัส โปรแกรม');
  define("_HISTORY_CUSTOM_LOT_TITLE", 'คะแนนและล็อตสะสมของเทรดเดอร์ได้ถูกอัปเดต');
  define("_HISTORY_CUSTOM_LOT_DESC", 'ทางบริษัทได้มีการอัปเดตคะแนนและล็อตสะสมของเทรดเดอร์ให้มีความถูกต้องตามระบบ สามารถสอบถามข้อมูล เพิ่มเติมได้ทุกช่องทาง หรือ Line : @GOFX');

  // ข้อตกลงและเงื่อนไข //
  define("_TERM_AND_CONDITIONS_HEAD_1", "Terms and Conditions");
  define("_TERM_AND_CONDITIONS_HEAD_2", "GOFX.com is delighted to offer special promotions to reward our valued traders. Boost your investments,
                                          win many prizes, and claim it right away. Our prizes include a wide variety of choices which includes cutting-edge gadgets,
                                          luxury car brands such as Maserati and Porsche, cash coupons, and many more prizes meticulously selected just for you.");
  define("_TERM_AND_CONDITIONS_HEAD_3", "Please carefully read the following terms and conditions. By participating in the Loyalty Program of GOFX.com, you agree to be
                                          bound by these program terms and conditions.");
  define("_TERM_AND_CONDITIONS_DETAIL_1", "A.	Terms and conditions of Loyalty Bonus Program");
  define("_TERM_AND_CONDITIONS_DETAIL_2", "1.	GOFX.com is under the operation of GOFX Limited, the security trader with registration No. 25865BC2020 having
                                          the registered address located at Suite 305, Griffith Corporate Centre, P.O. Box 1510, Beachmont, Kingstown,
                                          St. Vincent and the Grenadines.");
  define("_TERM_AND_CONDITIONS_DETAIL_3", "2. Loyalty Bonus Program is the special privilege promotion to reward the traders who use the service of GOFX.com.
                                          You will receive a lot of valuable rewards from GOFX Premium Product, trade learning course from GOTA Go Trade Academy,
                                          modern Gadget, to the niche cars in various brands such as Maserati and Porsche. Just trading and accumulating the points,
                                          you can exchange your cumulative points with the rewards as you desire. ");
  define("_TERM_AND_CONDITIONS_DETAIL_4", "B.	Definitions");
  define("_TERM_AND_CONDITIONS_DETAIL_5", "1.	Go Point is the point unit gained from trading under the Loyalty Bonus Program  </br>
                                          2.	GO Trader is the 1st level status. </br>
                                          3.	Advance Trader is the 2nd level status. </br>
                                          4.	Expert Trader is the 3rd level status. </br>
                                          5.	Master Trader is the 4th level status.</br>
                                          ");
  define("_TERM_AND_CONDITIONS_ARTICLE", "C. General provisions");
  define("_TERM_AND_CONDITIONS_ARTICLE_1", "1.	Loyalty Bonus Program Status. The Loyalty Bonus Program Status is the level of trader participating in
                                            the Loyalty Bonus Program. The elevation of level will depend on the accumulation of points (GO point) which
                                            will be calculated from the trading volume (Lot) of traders.");
  define("_TERM_AND_CONDITIONS_ARTICLE_2", "2.	In each Loyalty Bonus Program Status, the GO point will be given differently as follows:");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_1", "2.1 Loyalty bonus's point calculation table");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_1_DETAIL_1", "ตารางสถานะลอยัลตี้โบนัสต่อคะแนนที่ได้รับ");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_2", "2.2	For example, when trader is in the Go Trader status and trades for 10 Lots, the trader will receive
                                              (10 Lots X 10 GO point) = 100 GO point. When the trader is promoted to Advance Trader and trades for 10 Lots,
                                               the trader will receive (10 Lots X 12 GO point) = 120 GO point. ");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_3", "2.3 กรณี ‘บัญชี Low Spread’ จำนวนล็อตของลูกค้าจะถูก หารด้วย 10 กล่าวคือ 1 ล็อต ของบัญชี Low Spread จะได้รับคะแนนตามตารางดังต่อไปนี้");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_3_DETAIL_1", " Loyalty bonus's point calculation table : Low Spread account");
  define("_TERM_AND_CONDITIONS_ARTICLE_2_4", "2.4	In case of “Mini Account”, the Lot volume of the customers will be divided by 100 in order to change USDC
  to USD before calculated as points in the Loyalty Program. It can be said that 1 Lot of Mini Account will
  receive the points as show in the following table. ");
define("_TERM_AND_CONDITIONS_ARTICLE_2_4_DETAIL_1", " Loyalty bonus's point calculation table : Mini account");
  define("_TERM_AND_CONDITIONS_ARTICLE_3", "3.	All traders of GOFX.com who open the real accounts and have successfully verified the email and identity will
                                            be eligible to participate in the Loyalty Bonus Program automatically without any additional registration required.
                                            All traders will get Go Trader status as the default status.");
  define("_TERM_AND_CONDITIONS_ARTICLE_4", "4.	The trader’s reward points will be ‘reset’ as the initial cumulative points and lot of the current status if
                                            there is no account movement for a period of 6 months. The traders can check their account movement in the point
                                            accumulation history page.");
  define("_TERM_AND_CONDITIONS_ARTICLE_5", "5.	In the event that the trader’s account is ‘reset on the points’, the reset points cannot be recovered.
                                            However, the traders can newly start accumulating the points according to the conditions set by the Company.
                                            If the new trade has begun, the traders will not be able to receive the same reward as the one they redeemed
                                            before point reset. In order to continue receiving rewards, the traders have to trade and accumulate points
                                            to complete the equivalent of the latest points before being reset.");
  define("_TERM_AND_CONDITIONS_ARTICLE_6", "6.	Every trader will be upgraded to the Loyalty Bonus Program Status by accumulating lots from trading.
                                            If any trader makes a lot of trade, the Loyalty Bonus Program Status will also rise. The obtained points
                                            will also increase. The points for the number of traded lots are automatically distributed to the account
                                            of the trader program automatically (view the principles of point calculation in 2.1).");
  define("_TERM_AND_CONDITIONS_ARTICLE_6_1", "6.1	The GO point cannot be transferred to another type of account under the same name or other people’s accounts.้");
  define("_TERM_AND_CONDITIONS_ARTICLE_7", "7.	Reward redemption requirements. The points of the loyalty bonus program status are used to redeem special
                                            rewards for traders of GOFX.com. The calculation method of points will follow the formula (view 2.2).
                                            The redemption requirements are as follows:");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_1", "Traders can exchange points for various rewards following the current loyalty bonus program status.
                                              In redeeming the reward, the trader needs to have sufficient GO points and meet the specified conditions.");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_2", "If a trader has redeemed rewards based on their preferred loyalty bonus status, the points will not be
                                              reduced but will be increased over and over by the number of lots traded by the traders. Traders can enjoy
                                              unlocking exclusive rewards from time to time.");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_3", "Traders can proceed to request for redemption in Member Area by clicking on the reward they want and
                                              pressing proceed to request for redemption. Then they will be contacted by the staff within 1 day to
                                              inform the details of the reward redemption.");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_4", 'To “exchange points for money”, traders can claim cash coupons in the reward catalog.
                                              This coupon can be converted to money in the system and can be withdrawn.');
  define("_TERM_AND_CONDITIONS_ARTICLE_7_5", "After the requested reward has been approved, GOFX.com will deliver the prize within 30 working days.
                                              In case of any shortage of the reward, the operation may be delayed. The situation will always be informed.");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_6", "For all additional administrative expenses such as freight, taxes, etc., the reward recipient is solely
                                              liable for the difference by making a payment of 5% withholding tax before receiving the reward.");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_7", "GOFX.com is not responsible for any quality and post-sales service of any product given to reward recipient.");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_8", "GOFX.com reserves the right to use photos and videos of traders for promotional or marketing activities.");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_9", "If the Company cannot contact trader or the trader does not respond within 28 days, such reward will be canceled.");
  define("_TERM_AND_CONDITIONS_ARTICLE_7_10", "The Company will have schedule for updating the rewards every 6 months which may update all products or just some items.");
  define("_TERM_AND_CONDITIONS_ARTICLE_8", "8.	Last provision");
  define("_TERM_AND_CONDITIONS_ARTICLE_8_1", "GOFX.com reserves the right to change or update the terms and conditions without prior notice.");
  define("_TERM_AND_CONDITIONS_ARTICLE_8_2", "GOFX.com reserves the right to cancel awarding rewards to traders whose points originate as suspicious of suspicious behavior.
                                              Where the allegation is proven to be transparent, all of the suspended points will be automatically refunded to the trader’s account.");
  define("_TERM_AND_CONDITIONS_ARTICLE_8_3", "If a Trader does not wish to participate in the Loyalty Bonus Program, such trader can do so by contacting the GOFX.com Trader
                                              Service Center. GOFX.com will process the request and inform later.");
  define("_TERM_AND_CONDITIONS_ARTICLE_8_4", "By joining the Loyalty Bonus Program, the traders agree to accept the terms and conditions of the Loyalty Bonus Program.");

  // ติดต่อเรา //
  define("_CONTACT_HEAD", 'Contact Us');
  define("_CONTACT_HEADER_TITLE", '“ We connect people to money by the friendliest approach. ”');
  define("_CONTACT_HEADER_TEXT1", "GOFX (International) Limited");
  define("_CONTACT_HEADER_TEXT2", "believes finance is a matter of everyone. Thus, we provide support and activities to promote education of investment
                                  analysis techniques and personal financial planning techniques; and to develop trading system in accordance with needs
                                  and satisfaction toward trading orders that suit trader as well as investor needs in each particular area. We consider
                                  the priority of those countries which have a lot of investors who are interested in the trading platform services for
                                  profit from the market. For example, Singapore, Vietnam, Thailand, and the People’s Republic of Laos.");
  define("_CONTACT_SOCIAL_1", '02-026-6559');
  define("_CONTACT_SOCIAL_2", '@GOFX');
  define("_CONTACT_SOCIAL_3", 'support@gofx.com');
  define("_CONTACT_SOCIAL_4", 'GOFXthailand');
  define("_CONTACT_FOOTER_TEXT1", "GOFX.COM is managed and controlled by GOFX Limited who is a registered stockbroker. The registered number is 25865BC2020</br>
                                  with its registered address at Suite 305, Griffith Corporate Center, P.O.Box 1510, Beachmont, Kingstown, St. Louis. Vincent and the Grenadines.");
  define("_CONTACT_FOOTER_TEXT2", "GOFX Limited owns the copyright in all information displayed, images, and formats are shown on the website unless expressly stated otherwise.no
                                  one is allowed to copy, reproduce, imitate, reproduce, modify, add to which it is distributed for any other purpose, except receiving the written
                                  permission of the Company.");
  define("_CONTACT_FOOTER_TEXT3", "General risk warning: Contract for difference or CFD is leverage’s product. Therefore, trading with CFDs involves a high level of risk which may
                                  not be suitable for all investors. The value of an investment can either increase or decrease. Investors may lose their entire investment under any
                                  circumstances. The Company and its related entities are not responsible for any person or entity, for any loss or loss of all or part of their investment
                                  resulting from the execution of orders or in connection with any CFD-related transactions.");
  define("_CONTACT_FOOTER_TEXT4", "Risk warning : Our services involve a significant risk and can result in the loss of your invested capital.");
  define("_CONTACT_FOOTER_TEXT5", "Risk warning : Our services involve a significant risk and can result in the loss of your invested capital.");
?>
